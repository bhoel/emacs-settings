;;; system-work --- define system settings for gnus

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;;; Commentary:

;; Erst die globalen Einstellungen.  Wenige Ausnahmne werden in
;; `gnus-parameters' festgelegt.

;;; Code:

(eval-when-compile (require 'use-package))

(eval-when-compile
  (require 'gnus)
  (require 'gnus-msg)
  (require 'epg)
  (require 'nnir)
  (use-package nnir-est)
  (require 'smtpmail)
  (require 'spam)
  (require 'nnfolder)
  (require 'nnmail))

(require 'message)

(setq gnus-parameters
      '(("^nnimap:.*"
         (gcc-self . t))
        ("^gmane\\..*"
         (gnus-article-sort-functions '(gnus-article-sort-by-number))
         (gnus-thread-sort-functions '(gnus-thread-sort-by-number))
         (spam-contents gnus-group-spam-classification-ham)
         (spam-process ((spam spam-use-gmane)
                        (spam spam-use-bogofilter)
                        (ham spam-use-bogofilter))))

        ("^\\<\\(gnu\\|gnus\\|de\\|comp\\|alt\\|ger\\|linux\\|misc\\|pgsql\\|public\\|rec\\|schule\\|sci\\|spline\\|uk\\)\\..*\\>"
         (gnus-article-sort-functions '(gnus-article-sort-by-number))
         (gnus-thread-sort-functions '(gnus-thread-sort-by-number))
         (spam-process ((spam spam-use-gmane)
                        (spam spam-use-bogofilter)
                        (ham spam-use-bogofilter))))

        ("^nnfolder.*"
         (gnus-use-scoring nil))

        ("^nndraft.*"
         (gnus-use-scoring nil))

        ("^nnfolder:spam"
         (gnus-article-sort-functions '(gnus-article-sort-by-subject))
         (gnus-thread-sort-functions '(gnus-thread-sort-by-subject))
         (spam-contents gnus-group-spam-classification-spam)
         (spam-process ((spam spam-use-bogofilter))))))
;;    (".*"
;;     (gnus-article-sort-functions '(gnus-article-sort-by-number))
;;     (gnus-thread-sort-functions '(gnus-thread-sort-by-number))
;;     (spam-process ((spam spam-use-bogofilter)
;;                 (ham spam-use-bogofilter)))))

(setq gnus-spam-newsgroup-contents
 '(("^nnfolder:spam" gnus-group-spam-classification-spam)
   (".*" gnus-group-spam-classification-ham)))

(setq gnus-spam-process-newsgroups
 '(("^nnfolder:spam" ((spam spam-use-bogofilter)))))

(setq nnfolder-directory "~/Mail")

;; Mails senden und SMTP-Server definieren
;;
;; Mit folgenden Zeilen weiss Gnus wie und zu welchem SMTP-Server es
;; Emails senden soll:
(setq message-send-mail-function 'smtpmail-send-it)
(setq smtpmail-smtp-service 25)
;; (setq smtpmail-debug-info t)
;; (setq smtpmail-debug-verb t)
;; (setq smtpmail-default-smtp-server "mailhost.gl-group.com")
;; (setq smtpmail-smtp-server "mailhost")
(setq smtpmail-default-smtp-server "localhost")
(setq smtpmail-smtp-server "localhost")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; PGP(/MIME)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq epg-user-id "BE728B06EA464FEF")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Nützliche Funktionen
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;; automatisch signieren
;; (add-hook 'gnus-message-setup-hook 'mml-secure-sign-pgpmime)

(setq nnimap-record-commands t)

;; (setq tls-program
;;       '("ssh srverc gnutls-cli -p %p %h"
;;         "gnutls-cli --insecure -p %p %h"
;;         ;; "gnutls-cli --insecure -p %p %h --protocols ssl3"
;;         "gnutls-cli --x509cafile %t -p %p %h"
;;         "gnutls-cli --x509cafile %t -p %p %h --protocols ssl3"
;;         "openssl s_client -connect %h:%p -no_ssl2 -ign_eof"))

(setq gnus-select-method
      '(nnimap "imap"
               (nnimap-record-commands t)
               (nnimap-user "berthold.hoellmann@kumkeo.de")
               (nnimap-address "outlook.office365.com")
               (nnimap-server-port 993)
               (nnimap-stream tls)
               ;;(nnimap-stream ssl)
               ;;(nnimap-streaming nil)
               (nnir-search-engine imap)
               (nnimap-inbox "INBOX")
               (nnimap-split-methods nnmail-split-fancy)))

(setq gnus-secondary-select-methods
      '((nnfolder "")
        (nntp "srverc.germanlloyd.org")))

;; Hack for posting on foreign servers
(add-hook 'message-setup-hook
          (lambda ()
            (local-set-key "\C-c\C-c" 'message-send-and-exit-with-prefix)))

(defun message-send-and-exit-with-prefix ()
  "Call the `message-send-and-exit' function with a positive
number argument to make it post the message on the foreign NNTP
server of a group, instead of the default NNTP server."
  (interactive)
  (message-send-and-exit 1))

;; -- posting styles --
;; Customize your name, addresses and signature files.
(setq gnus-posting-styles
      '((".*"
         ;; Default, add lines which should be in all you mails and postings
         (organization "kumkeo GmbH <http://www.kumkeo.de>")
         ;;("X-Headers" "just as an example")
         (name "Berthold Hoellmann")
         (eval (setq ispell-local-dictionary "american")))
        (".*.de"
         (eval (setq ispell-local-dictionary "german")))
        (".*@kumkeo.de"
         (eval (setq ispell-local-dictionary "german")))
        ("software.gsrc.bug"
         (name "Berthold Hoellmann")
         (eval (setq ispell-local-dictionary "american")))
        ;; Here is where you put your private email address
        (message-mail-p
         ;; (from "Joe Ding <joe@private.com>")
         (signature-file "~/.signature_mail"))
        ;; If you have special setup for separate groups, here's how.
        ;; The ^ means beginning of line, $ means end of line.
        ;; The two \\ escapes special characters to take away their special behavior
        ;;  ("^mail\\.work$"
        ;;   (signature-file "~/signature.work")
        ;;   )
        ;; You can have a different news address, if you want.
        ;; Usenet appreciates valid addresses!
        (message-news-p
         ;; (from "Joe Ding <news@private.com>")
         (address "berthold@despammed.com")
         ;; Die Reply-To-Adresse, wenn du keine haben möchtest, lösch
         ;; die Zeile einfach.
         ("Reply-To" "<berthold.hoellmann@kumkeo.de>")
         (signature-file  "~/.signature"))
        ;;  ("^gnu.emacs.gnus$"
        ;;   (signature-file  "~/.signature.gnus")
        ;;   )
        ))

(setq nnir-method-default-engines
      '((nnimap . imap)
        (nnmaildir . est)
        (nnfolder . est)
        (nnml . est)
        (nntp . gmane)))
(use-package nnir-est
  :init
  (setq nnir-est-prefix "/home/berhol/Mail/"))

;; gnus-message-archive-group
(setq gnus-message-archive-method
      '(nnfolder "archive"
                 (nnfolder-inhibit-expiry t)
                 (nnfolder-active-file "~/News/sent-mail/active")
                 (nnfolder-directory "~/News/sent-mail/")))

;; Documentation:
;; *Name of the group in which to save the messages you've written.
;; This can either be a string; a list of strings; or an alist
;; of regexps/functions/forms to be evaluated to return a string (or a list
;; of strings).  The functions are called with the name of the current
;; group (or nil) as a parameter.

;; If you want to save your mail in one group and the news articles you
;; write in another group, you could say something like:
(setq gnus-message-archive-group
      '((if (message-news-p)
            "misc-news"
          "misc-mail")))

(unless (member system-type (list 'windows-nt 'ms-dos))
  (setq spam-bogofilter-database-directory "/tmp/berhol/.bogofilter/"))

(provide 'my-gnus/system-work)



;; Local Variables:
;; mode:emacs-lisp
;; mode:flyspell
;; coding:utf-8
;; End:

;;; system-work.el ends here
