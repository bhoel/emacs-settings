;;; my-gnus/split-work --- Splitting rules for gnus at work.

;;; Commentary:

;; Copyright (C) 2013, 2022 by Berthold Höllmann

;; Task      define settings for gnus splitting of incoming mails

;; Author: Berthold Höllmann <berhoel@gmail.com>

;;; Code:

(eval-when-compile
  (require 'use-package))

(use-package nnimap)

;; Whether the nnmail splitting functionality should MIME decode headers.
(setq nnmail-mail-splitting-decodes t)


(provide 'my-gnus/split-work)
;;; split-work.el ends here



;; Local Variables:
;; mode: emacs-lisp
;; coding: utf-8
;; End:
