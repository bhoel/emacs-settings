;;; system-kumkeo.el --- Define system settings for `gnus'.

;; Copyright (C) 2020, 2022 by Berthold Höllmann

;; Task      define system settings for gnus

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Code:

(provide 'my-gnus/system-kumkeo)



;; Local Variables:
;; mode:emacs-lisp
;; mode:flyspell
;; coding:utf-8
;; End:

;;; system-kumkeo.el ends here
