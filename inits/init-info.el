;;; init-info.el --- Settings for `info'.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:12 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

;;; info.el --- info package for Emacs
(use-package info
  :bind ("C-h i" . info)
  :init
  (dolist
      (dir
       '("/opt/share/info/"
         "/usr/share/info/"
         "/usr/local/info/"
         "/usr/local/share/info/"
         "~/share/info/"))
    (progn
      (when (file-accessible-directory-p (expand-file-name dir))
        (add-to-list 'Info-additional-directory-list (expand-file-name dir) t)))))

;;; niceify-info --- improve usability of Info pages
;; Emacs' Info manuals are extremely rich in content, but the user
;; experience isn't all that it could be; an Emacs process contains a
;; lot of information about the same things that Info manuals
;; describe, but vanilla Info mode doesn't really do much to take
;; advantage of that. Niceify-info remedies this.
(use-package niceify-info
  :ensure t
  :hook (Info-selection . niceify-info))

(el-init-provide)



;;; init-info.el ends here
