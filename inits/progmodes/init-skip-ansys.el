;;; init-ansys.el --- settings for ansys mode

;; Copyright © 2003, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:34 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'init-autoinsert)
(require 'init-progmodes)

;;; ansys-mode.el -- Editor support for working with ANSYS FEA.
(use-package apdl-mode
  :ensure t

  ;;; Autoloading
  ;; Set of useful commands which are now interactively available (M-x
  ;; ...)  even when ANSYS Mode was not (yet) activated i.e. the lisp
  ;; files not loaded.
  :commands ansys-mode ansys-customise-ansys ansys-abort-file ansys-display-error-file
  :commands ansys-start-ansys-help ansys-start-ansys ansys-license-status

  ;; File suffixes for autoloading of `ansys-mode', appropriate file
  ;; suffixes for which ANSYS mode is automatically called for. `.mac'
  ;; is the macro suffix of ANSYS i. e. these macros can be called in
  ;; the ANSYS command prompt like a regular ANSYS function (without
  ;; the suffix `.mac'). `.dat' and `.inp' are WorkBench's solver
  ;; input file suffixes, `.anf' is the suffix for "ANSYS Neutral"
  ;; files which include mostly gometric data but also some APDL
  ;; snippets.
  ;;
  :mode ((rx (or ".mac" ".dat" ".inp" ".ansys" ".anf") eos))
  :hook ((find-file . auto-insert)
         ;;; Outlining
         ;; activating outline minor mode for selectively hiding/unhiding
         ;; sections
         (ansys-mode . ansys-outline-minor-mode))
  :init
  (setq ansys-help "/ansys_inc/v120/ansys/bin/anshelp120")
  (add-to-list 'auto-insert-alist
               '(ansys-mode . [ansys-skeleton-outline-template]))


  :config
  ;;; Fontification
  ;; fontification (highlighting) of user variables and decoration
  ;; levels (0,1,2 are available), uncommenting the following might
  ;; slow the editing of large .mac files (but only when
  ;; ansys-highlighting-level is set to 2).
  (setq ansys-dynamic-highlighting-flag t)

  ;; experimental user variables highlighting is in level 2 available
  ;; (statical if above flag is not set), the default is 2
  (setq ansys-highlighting-level 2)

  ;;; ANSYS processes stuff

  ;; IMPORTANT things you probably have to configure:

  ;; which version of ANSYS we are using, the current version is "145"
  (setq ansys-current-ansys-version "120")
  ;; (setq ansys-current-ansys-version "130")
  ;; (setq ansys-current-ansys-version "140")
  ;; (setq ansys-current-ansys-version "150")

  ;; Number of cores for the run, 2 does not require HPC licenses
  (setq ansys-no-of-processors 2)

  ;; ANSYS installation directory

  (cond ((string= window-system "x")
         ;; "/" the root dir is the default installation directory on Gnu/Linux
         (setq ansys-install-directory "/"))
        (t
         ;; the default is "C:\\Program Files" on Windows
         (setq ansys-install-directory "C:\\Program Files\\")))

  ;; license server configuration and executable paths
  (cond ((string= window-system "x")
  ;;; Gnu/Linux 64 bit
         ;; for starting the solver & ansys-license-status & ANSYS help

         ;; license servers (or license file name)
         ;; specify even the default port for lmutil (since ANSYS V
         ;; 12.0) on Gnu/Linux Gnu/Linux: License servers separated
         ;; by colons (":"), 1055 is the default port
         (setq ansys-license-file "1055@chi:1055@research")

         ;; since ANSYS 12.0 there is an intermediate server for the
         ;; communication between flexlm and ANSYS, 2325 is the
         ;; default port
         (setq ansys-ansysli-servers "2335@chi:2325@research"))
        (t
         ;; else Windows 64 bit

         ;; the following is now executed in the mode function
         ;; ansys-mode itself, dropped 32 bit support
         ))

  ;; options when starting the solver below example shows the ANSYS
  ;; mode defaults
  ;; which license type to use for the solver
  (setq ansys-license "struct")
  ;; ANSYS job name
  (setq ansys-job "file")

  ;;; Auto insertion
  ;; auto insertion stuff (when creating a new APDL file)
  (auto-insert-mode 1)) ;; switch global minor-mode on

(defun my@insert@mac@file@template(&rest args)
  "Insert yas file template for .mac files."
  (my-yas-expand-by-uuid 'ansys-mode "file_template"))
(define-auto-insert (rx ".mac" eos) #'my@insert@mac@file@template)

(el-init-provide)



;;; init-ansys.el ends here
