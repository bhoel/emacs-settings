;;; init-ggo-mode.el --- Settings for `ggo-mode'.

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:30 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'init-progmodes)
(require 'init-yasnippet)
(require 'init-autoinsert)

;;; ggo-mode.el --- Gengetopt major mode
(use-package ggo-mode
  :ensure t
  :mode (rx ".ggo" eos)
  :config
  (define-skeleton my-ggo-skeleton
    "Skeleton for Gengetopt files."
    "Package name: "
    "# $Id\$" n
    '(comment-region
      (point)
      (progn
        (my-blurb-head)
        (point)) 2)

    \n "# gengetopt input file.  See the info entry for directions."
    \n "package \"" str "\""
    \n "# version \"1.0\""
    \n "purpose \"" (skeleton-read "Purpose: " nil nil) "\""
    \n (skeleton-insert '("Usage: " "usage \"" str & "\"" | -7))
    \n (skeleton-insert '("Description: " "description \"" str & "\"" | -13))
    \n
    _
    \n "option \"parser-debug\" P \"Debug the parser.\" no"
    \n "option \"scanner-debug\" S \"Debug the scanner.\" no"
    \n "text \"\\n\\nReport bugs to <" user-mail-address ">.\"")
  (add-to-list 'auto-insert-alist '(ggo-mode . my-ggo-skeleton)))

(el-init-provide)



;;; init-ggo-mode.el ends here
