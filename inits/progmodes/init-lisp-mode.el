;;; lisp-mode-settings.el --- Settings for `lisp-mode'.

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2024-01-13 11:40:34 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(require 'init-my-switches)
(require 'init-autoinsert)
(require 'init-progmodes)
(require 'init-whitespace)
(require 'init-yasnippet)

;;; lisp-mode --- Lisp mode, and its idiosyncratic commands
(use-package lisp-mode
  :mode (((rx ".elc" eos) . emacs-lisp-byte-code-mode)
         ((rx (or ".lisp" ".l" ".lip" ".ml" ".asd") eos) . lisp-mode)))

;;; sly-repl-ansi-color --- Add ANSI colors support to the sly mrepl.
(use-package sly-repl-ansi-color
  :after sly
  :ensure t
  :config
  (push 'sly-repl-ansi-color sly-contribs))

;;; helm-sly.el --- Helm sources and some utilities for SLY.
(use-package helm-sly
  :ensure t
  :after sly helm-company
  :hook (mrepl-hook . company-mode)
  :bind (
	 :map sly-mrepl-mode-map
	 (("<tab>" . helm-company))))

;; The major mode for editing Emacs Lisp code.
;; This mode is documented in the Emacs manual.
(use-package elisp-mode
  :commands lisp-interaction-mode
  :mode (("\\.el\\'" . emacs-lisp-mode)
         ("Project\\.ede\\'" . emacs-lisp-mode)
         ("[]>:/\\]\\..*\\(emacs\\|gnus\\|viper\\)\\'" . emacs-lisp-mode)
         ("\\`\\..*emacs\\'" . emacs-lisp-mode)
         ("[:/]_emacs\\'" . emacs-lisp-mode)))

(defun my@insert@el@head (&rest args)
  "Insert yas head for .el files. Ignore ARGS."
  (my-yas-expand-by-uuid 'emacs-lisp-mode "head"))
(define-auto-insert (rx ".el" eos) #'my@insert@el@head)

;;; checkdoc --- check documentation strings for style requirements
(use-package checkdoc
  :hook (emacs-lisp-mode . checkdoc-minor-mode))

;;; ielm --- interaction mode for Emacs Lisp
(use-package ielm
  :after elisp-mode
  :commands inferior-emacs-lisp-mode)

;;; eval-in-repl-ielm.el --- ESS-like eval for ielm
;; ielm support (for emacs lisp)
(use-package eval-in-repl-ielm
  :ensure eval-in-repl
  :after ielm
  :if my-isux
  :bind
  (;; for .el files
   :map emacs-lisp-mode-map (([C-return] . eir-eval-in-ielm))
   ;; for *scratch*
   :map lisp-interaction-mode-map ([C-return] . eir-eval-in-ielm)
   ;; for M-x info
   :map Info-mode-map ([C-return] . eir-eval-in-ielm))
  :init
  ;; Evaluate expression in the current buffer.
  (setq eir-ielm-eval-in-current-buffer t))

;;; init-flycheck --- Settings for `flycheck'.
(require 'init-flycheck)

;;; morlock --- more font-lock keywords for elisp
;; This library defines more font-lock keywords for Emacs lisp.
(use-package morlock
  :ensure t
  :after elisp-mode
  :config
  (global-morlock-mode))

;;; lisp-extra-font-lock --- Highlight bound variables and quoted
;;;                          exprs.
;; This package highlight the location where local variables is
;; created (bound, for example by `let') as well as quoted and
;; backquoted constant expressions.
(use-package lisp-extra-font-lock
  :ensure t
  :hook (emacs-lisp-mode . lisp-extra-font-lock-global-mode))

;;; common-lisp-snippets --- Yasnippets for Common Lisp
;; License GPL 3 This is a collection of Yasnippets for Common Lisp.
;; It mainly includes snippets for top-level forms and (as a bonus)
;; headers for popular free-software licenses: GNU GPL and MIT
;; License.
(use-package common-lisp-snippets
  :ensure t
  :after (:all yasnippet elisp-mode))

;;; Srefactor --- A refactoring tool based on Semantic parser
;;;               framework
(use-package srefactor-lisp
  :ensure srefactor
  :bind (
         :map lisp-mode-shared-map
         ("M-RET o" . srefactor-lisp-one-line)
         ("M-RET m" . srefactor-lisp-format-sexp)
         ("M-RET d" . srefactor-lisp-format-defun)
         ("M-RET b" . srefactor-lisp-format-buffer)))

;;; adjust-parens --- Indent and dedent Lisp code, automatically
;;;                   adjust close parens
;; This package provides commands for indenting and dedenting Lisp
;; code such that close parentheses and brackets are automatically
;; adjusted to be consistent with the new level of indentation.
;;
;; When reading Lisp, the programmer pays attention to open parens
;; and the close parens on the same line. But when a sexp spans more
;; than one line, she deduces the close paren from indentation
;; alone. Given that's how we read Lisp, this package aims to enable
;; editing Lisp similarly: automatically adjust the close parens
;; programmers ignore when reading. A result of this is an editing
;; experience somewhat like python-mode, which also offers "indent"
;; and "dedent" commands. There are differences because lisp-mode
;; knows more due to existing parens.
(use-package adjust-parens
  :ensure t
  :after elisp-mode
  :bind (
         :map lisp-mode-shared-map
         ([tab] . lisp-indent-adjust-parens)
         ([backtab] . lisp-dedent-adjust-parens))
  ;;
  ;; lisp-indent-adjust-parens potentially calls indent-for-tab-command
  ;; (the usual binding for TAB in Lisp Mode). Thus it should not
  ;; interfere with other TAB features like completion-at-point.
  ;;
  ;; Some examples follow. | indicates the position of point.
  ;;
  ;;   (let ((x 10) (y (some-func 20))))
  ;;   |
  ;;
  ;; After one TAB:
  ;;
  ;;   (let ((x 10) (y (some-func 20)))
  ;;     |)
  ;;
  ;; After three more TAB:
  ;;
  ;;   (let ((x 10) (y (some-func 20
  ;;                              |))))
  ;;
  ;; After two Shift-TAB to dedent:
  ;;
  ;;   (let ((x 10) (y (some-func 20))
  ;;         |))
  ;;
  ;; When dedenting, the sexp may have sibling sexps on lines below.
  ;; It makes little sense for those sexps to stay at the same
  ;; indentation, because they cannot keep the same parent sexp
  ;; without being moved completely. Thus they are dedented too. An
  ;; example of this:
  ;;
  ;;   (defun func ()
  ;;     (save-excursion
  ;;       (other-func-1)
  ;;       |(other-func-2)
  ;;       (other-func-3)))
  ;;
  ;; After Shift-TAB:
  ;;
  ;;   (defun func ()
  ;;     (save-excursion
  ;;       (other-func-1))
  ;;     |(other-func-2)
  ;;     (other-func-3))
  ;;
  ;; If you indent again with TAB, the sexps siblings aren't
  ;; indented:
  ;;
  ;;   (defun func ()
  ;;     (save-excursion
  ;;       (other-func-1)
  ;;       |(other-func-2))
  ;;     (other-func-3))
  ;;
  ;; Thus TAB and Shift-TAB are not exact inverse operations of each
  ;; other, though they often seem to be.
  :hook ((emacs-lisp-mode lisp-mode) . adjust-parens-mode))

;;; elisp-def --- macro-aware go-to-definition for elisp
;; Find the definition of the symbol at point,
;; intelligently.  Understands namespaces, macros, libraries and local
;; bindings.
;; See full docs at https://github.com/Wilfred/elisp-def
(use-package elisp-def
  :ensure t
  :if my-isux
  :hook ((emacs-lisp-mode ielm-mode) . elisp-def-mode))

;; ert --- Emacs Lisp Regression Testing
(use-package ert
  :commands ert-deftest)

(el-init-provide)



;;; init-lisp-mode.el ends here
