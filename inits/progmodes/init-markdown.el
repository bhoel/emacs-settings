;;; init-markdown.el --- Settings for edition markdown files.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:36 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

;;; markdown-mode.el --- Emacs Major mode for Markdown-formatted text
;;;                      files
;; Major mode for editing Markdown files
(use-package markdown-mode
  :ensure t
  :bind (
         :map markdown-mode-map
         ([C-f8] . format-all-buffer))
  :mode ((rx (or ".md") eos)))

;;; markdown-toc.el --- A simple TOC generator for markdown file
;; Generate a TOC from a markdown file: M-x markdown-toc/generate-toc
;; This will compute the TOC at insert it at current position.
;; Update existing TOC: C-u M-x markdown-toc/generate-toc
(use-package markdown-toc
  :ensure t
  :after markdown-mode
  :hook (markdown-mode . markdown-toc-mode)
  :commands markdown-toc-version ;; Markdown-toc version. Generate a
  ;; TOC for markdown file at current point. Deletes any
  ;; previous TOC.
  ;; If called interactively with prefix arg REPLACE-TOC-P,
  ;; replaces previous TOC.
  :commands markdown-toc-generate-toc)

;;; pandoc-mode.el --- Minor mode for interacting with Pandoc  -*- lexical-binding: t -*-
;; Pandoc-mode is a minor mode for interacting with Pandoc, a 'universal
;; document converter': <http://johnmacfarlane.net/pandoc/>.
;;
;; See the pandoc-mode manual for usage and installation instructions.
(use-package pandoc-mode
  :ensure t
  :hook (markdown-mode . pandoc-mode))

(el-init-provide)



;;; init-markdown.el ends here
