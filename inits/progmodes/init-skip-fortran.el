;;; init-fortran.el --- Settings for editing FORTRAN code.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:33 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package)
  (require 'rx))

(require 'el-init)

(require 'init-my-switches)
(require 'init-abbrev)
(require 'init-autoinsert)
(require 'init-progmodes)
(require 'init-spell)

;;; f90.el --- Fortran-90 mode (free format)
;; Major mode for editing Fortran 90 code.
(use-package f90
  :commands f90-add-imenu-menu f90-change-keywords
  :mode ((rx (or ".f90" ".F90" ".f95" ".F95") eos) . f90-mode)
  :config
  (setq f90-do-indent 3)
  (setq f90-if-indent 3)
  (setq f90-type-indent 3)
  (setq f90-program-indent 2)
  (setq f90-continuation-indent 5)
  (setq f90-comment-region "!!$")
  (setq f90-directive-comment-re "!hpf\\$")
  (setq f90-indented-comment-re "!")
  (setq f90-break-delimiters "[-+\\*/><=,% \t]")
  (setq f90-break-before-delimiters t)
  (setq f90-beginning-ampersand t)
  (setq f90-smart-end 'blink)
  (setq f90-auto-keyword-case 'upcase-word)
  (setq f90-leave-line-no nil)
  (setq f90-font-lock-keywords f90-font-lock-keywords-2)
  ;; extra menu with functions etc.
  (f90-add-imenu-menu)        ; extra menu with functions etc.
  ;; change case of all keywords on startup
  (when f90-auto-keyword-case
    (f90-change-keywords f90-auto-keyword-case)))

;;; fortran.el --- Fortran mode for GNU Emacs
;; Major mode for editing FORTRAN code.
(use-package fortran
  :mode ((rx (or ".f" ".F" ".for" ".FOR") eos) . fortran-mode)
  :init
  (setq fortran-blink-matching-if t)
  (setq fortran-check-all-num-for-matching-do t)
  (setq fortran-comment-indent-style 'relative)
  (setq fortran-continuation-string "&"))

;;; fortpy.el --- a Fortran auto-completion for Emacs
(use-package fortpy
  :ensure t
  :if my-isux
  :hook ((fortran-mode f90-mode) . fortpy-mode)
  :init
  (require 'init-python))

(defun my@insert@f90@file@template(&rest args)
  "Insert yas file template for .f90 files. Ignore ARGS."
  (my-yas-expand-by-uuid 'f90-mode "file_template"))
(defun my@insert@for@file@template(&rest args)
  "Insert yas file template for .for files. Ignore ARGS."
  (my-yas-expand-by-uuid 'fortran-mode "file_template"))

(define-auto-insert (rx (or ".f90" ".F90") eos)
  #'my@insert@f90@file@template)
(define-auto-insert (rx (or ".f" ".F" ".FOR" ".for") eos)
  #'my@insert@for@file@template)



(el-init-provide)



;;; init-fortran.el ends here
