;;; init-matlab.el --- Settings for matlab mode.

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:23 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'init-progmodes)

;;; octave.el --- editing octave source files under emacs
;; only if `matlab-mode' is not available.
(use-package octave
  :disabled
  :mode ((rx ".m" eos) . octave-mode))

;;; matlab.el --- Auto-generated CEDET autoloads
(use-package matlab
  :ensure matlab-mode
  :mode ((rx ".m" eos) . octave-mode)
  :defines matlab-indent-function-body
  :config
  ;; if you want function bodies indented
  (setq matlab-indent-function-body t)
  ;; where auto-fill should wrap
  (setq fill-column 76)
  (turn-on-auto-fill))

(defun my@insert@m@file@template(&rest args)
  "Insert yas file template for .m files. Ignore ARGS."
  (my-yas-expand-by-uuid 'matlab-mode "file_template"))
(define-auto-insert (rx ".m" eos) #'my@insert@m@file@template)

(el-init-provide)



;;; init-matlab.el ends here
