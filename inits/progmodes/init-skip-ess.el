;;; init-ess.el --- Settings for `ess-mode'.

;; Copyright © 2014, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:34 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'init-my-switches)

(require 'init-company)

;;; ess-site.el --- user customization of ESS
(use-package ess-site
  :ensure ess
  :if my-isux
  :commands S R
  :init
  (when my-iswin
    (setq ess-program-files "c:/PROGRA~2")
    (setq ess-program-files-64 "c:/PROGRA~1")))

;;; r-autoyas --- Provides automatically created yasnippets for R
;;; function argument lists.
(use-package r-autoyas
  :ensure t
  :hook (ess-mode . r-autoyas-ess-activate))

(el-init-provide)



;;; init-ess.el ends here
