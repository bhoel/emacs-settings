;;; init-progmodes.el --- Common settings for programming modes.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:39:56 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(require 'eieio-core)

(require 'init-misc)
(require 'init-my-switches)
(require 'init-spell)
(require 'init-svn)
(require 'init-whitespace)
(require 'init-helm)

;;; prog-mode.el --- Generic major mode for programming
(use-package prog-mode)

;;; newcomment.el --- (un)comment regions of buffers
;;
;; This library contains functions and variables for commenting and
;; uncommenting source code.
(use-package newcomment
  :custom
  (comment-style 'extra-line))

;;; easy-escape --- Improve readability of escape characters in regular
;;;                 expressions
;; `easy-escape-minor-mode' composes double backslashes (escape characters) into
;; single backslashes, and highlights them to improve readability.
;;
;; For example, `easy-escape` displays "\\(?:\\_<\\\\newcommand\\_>\\s-*\\)?"
;; as "\(?:\_<\\newcommand\_>\s-*\)?".  The underlying text is not modified.
;;
;; The default it to use a single \ character instead of two, but the character
;; used and its color can be customized using `easy-escape-face' and
;; `easy-escape-character' (which see).
(use-package easy-escape
  :ensure t
  :if (not my-iswin)
  :hook (prog-mode . easy-escape-minor-mode)
  :custom
  (easy-escape-character ?⑊))

;;; electric-operator.el --- Automatically add spaces around operators
(use-package electric-operator
  :ensure t
  :hook (prog-mode . electric-operator-mode))

;;; unicode-troll-stopper --- Minor mode for Highlighting Unicode homoglyphs
;; Replace a semicolon (;) with a greek question mark (;) in your
;; friend's C# code and watch them pull their hair out over the syntax
;; error
(use-package unicode-troll-stopper
  :ensure t
  :hook (prog-mode . unicode-troll-stopper-mode))

;;; rainbow-delimiters --- Highlight nested parens, brackets, braces a
;;;                        different color at each depth.
;; Rainbow-delimiters is a "rainbow parentheses"-like mode which
;; highlights parentheses, brackets, and braces according to their
;; depth. Each successive level is highlighted in a different color.
;; This makes it easy to spot matching delimiters, orient yourself in
;; the code, and tell which statements are at a given level.
;;
;; Great care has been taken to make this mode FAST. You shouldn't see
;; any discernible change in scrolling or editing speed while using
;; it, even in delimiter-rich languages like Clojure, Lisp, and
;; Scheme.
;;
;; Default colors are subtle, with the philosophy that syntax
;; highlighting shouldn't be visually intrusive. Color schemes are
;; always a matter of taste. If you take the time to design a new
;; color scheme, please share (even a simple list of colors works) on
;; the EmacsWiki page or via github. EmacsWiki:
;; http://www.emacswiki.org/emacs/RainbowDelimiters Github:
;; http://github.com/jlr/rainbow-delimiters
(use-package rainbow-delimiters
  :ensure t
  :hook (prog-mode . rainbow-delimiters-mode))

;;; column-enforce-mode.el --- Highlight text that extends beyond a  column
;;  Highlight text that extends beyond a certain column (80 column rule)
;;
;;  By default, text after 80 columns will be highlighted in red
;;
;;  To customize behavior, see `column-enforce-column' and `column-enforce-face'
;;
;;  To enable: M-x column-enforce-mode
(use-package column-enforce-mode
  :ensure t
  :hook prog-mode
  :custom
  (column-enforce-column 88))

;;; xref.el --- Cross-referencing commands
(use-package xref)

;;; gxref --- xref backend using GNU Global.
;; A pretty simple (but, at least for me, effective) backend for xref
;; library, using GNU Global.
(use-package gxref
  :ensure t
  :after xref
  :functions gxref-xref-backend
  :config
  (add-hook 'xref-backend-functions #'gxref-xref-backend t))

;;; helm-xref --- Helm interface for xref results
(use-package helm-xref
  :ensure t
  :after (:all helm xref)
  :custom
  (xref-show-xrefs-function 'helm-xref-show-xrefs))

;;; debug.el --- debuggers and related commands for Emacs
(use-package debug
  :after helm-mode
  :config
  (add-to-list 'helm-completing-read-handlers-alist '(cancel-debug-on-entry)))

;;; RealGUD --- A extensible, modular GNU Emacs front-end for
;;;             interacting with external debuggers
(use-package realgud
  :ensure t
  :if my-isux
  :config
  (advice-add 'realgud:gdb :before #'tool-bar-mode)
  (advice-add 'realgud:pdb :before #'tool-bar-mode))

;;; dynamic-spaces --- When editing, don't move text separated by
;;;                    spaces
;; When editing a text, and `dynamic-spaces-mode' is enabled, text
;; separated by more than one space doesn't move, if possible.
;; Concretely, end-of-line comments stay in place when you edit the
;; code and you can edit a field in a table without affecting other
;; fields.
;;
;; For example, this is the content of a buffer before an edit (where
;; `*' represents the cursor):
;;
;;     alpha*gamma         delta
;;     one two             three
;;
;; When inserting "beta" without dynamic spaces, the result would be:
;;
;;     alphabeta*gamma         delta
;;     one two             three
;;
;; However, with `dynamic-spaces-mode' enabled the result becomes:
;;
;;     alphabeta*gamma     delta
;;     one two             three
(use-package dynamic-spaces
  :ensure t
  :hook (prog-mode . dynamic-spaces-mode))

;;; prog-fill --- Smartly format lines to use vertical space.
(use-package prog-fill
  :ensure t
  :bind (
	 :map prog-mode-map
	 ("M-q" . prog-fill)))

;;; format-all.el --- Auto-format C, C++, JS, Python, Ruby and 30 other languages
;; Lets you auto-format source code in many languages using the same
;; command for all languages, instead of learning a different Emacs
;; package and formatting command for each language.
;;
;; Just do M-x format-all-buffer and it will try its best to do the
;; right thing.  To auto-format code on save, use the minor mode
;; format-all-mode.  Please see the documentation for that function
;; for instructions.
(use-package format-all
  :ensure t
  :bind (
	 :map prog-mode-map
	 ([C-f8] . format-all-buffer)))

;;; highlight-doxygen --- Highlight Doxygen comments
(use-package highlight-doxygen
  :ensure t
  :functions highlight-doxygen-mode highlight-doxygen-global-mode
  :config
  ;; You can enable the minor mode for all major modes specified in
  ;; `highlight-doxygen-modes'.
  (highlight-doxygen-global-mode 1)
  :custom
  ;; List of major modes where Highlight Doxygen Global mode should be
  ;; enabled.
  (add-to-list 'highlight-doxygen-modes 'f90-mode)
  (add-to-list 'highlight-doxygen-modes 'fortran-mode)
  (add-to-list 'highlight-doxygen-modes 'python-mode))

(el-init-provide)



;;; init-progmodes.el ends here
