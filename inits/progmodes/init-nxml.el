;;; init-nxml.el --- Settings for `nxml-mode'.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:27 hoel>

;;; Commentary:

;;; Code:

;; Folding with Nxml Mode

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(require 'init-web)
(require 'init-flycheck)

;;; nxml-mode.el --- a new XML mode
(use-package nxml-mode
  ;; pom files should be treated as xml files
  :after hideshow
  :mode (rx (or ".pom" ".xml" ".html") eos)
  :magic (("<\\?xml" . nxml-mode)
	  ("<\\?html" . nxml-mode))
  :bind (:map nxml-mode-map
	 ([f7] . hs-toggle-hiding)
	 ([C-f8] . prettier-prettify))
  :custom
  (nxml-auto-insert-xml-declaration-flag t)
  (nxml-slash-auto-complete-flag t)
  :config
  (add-to-list 'hs-special-modes-alist
	       (list 'nxml-mode
		     "<!--\\|<[^/>]*[^/]>"
		     "-->\\|</[^/>]*[^/]>"
		     "<!--"
		     'nxml-forward-element
		     nil)))

;;; svgo.el --- SVG optimization with SVGO
(use-package svgo
  :bind(:map nxml-mode-map
	("M-o" . svgo)
	:map image-mode-map
	("M-o" . svgo)))

(el-init-provide)



;;; init-nxml.el ends here
