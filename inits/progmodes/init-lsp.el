;;; init-lsp.el --- Settings for LSP mode

;; Copyright © 2020, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:40 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;;; lsp-mode.el --- Emacs client/library for the Language Server Protocol
(use-package lsp-mode
  :ensure t
  :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
         ;; if you want which-key integration
         (lsp-mode . lsp-enable-which-key-integration))
  :commands lsp lsp-stdio-connection make-lsp--client lsp-register-client
  :custom
  ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
  (lsp-keymap-prefix "s-l")
  (lsp-idle-delay 0.1))

;;; lsp-diagnostics.el --- LSP diagnostics integration
(use-package lsp-diagnostics
  :ensure lsp-mode
  :after lsp-mode
  :custom
  (lsp-diagnostics-provider :flycheck)
  (lsp-signature-auto-activate t)
  (lsp-signature-doc-lines 1))

;; optionally
(use-package lsp-ui
  :ensure t
  :commands lsp-ui-mode)

;; if you are helm user
(use-package helm-lsp
  :after (helm)
  :ensure t
  :commands helm-lsp-workspace-symbol)

(use-package lsp-treemacs
  :ensure t
  :functions lsp-treemacs-sync-mode
  :defines lsp-treemacs-sync-mode
  :config
  (lsp-treemacs-sync-mode 1))

(use-package lsp-latex
  :ensure t
  :after (:any tex-mode yatex)
  :hook (((tex-mode latex-mode-hook yatex-mode) . lsp)))


;; optionally if you want to use debugger
(use-package dap-mode :ensure t :defer)
(use-package dap-cpptools :ensure dap-mode :after lsp-mode)

;; Native Debug (GDB/LLDB)
(use-package dap-gdb-lldb :ensure dap-mode)
(use-package dap-python :ensure dap-mode)
;; Usage
;;
;; A template named "Python :: Run Configuration" will appear, which
;; will execute the currently visited module. This will fall short
;; whenever you need to specify arguments, environment variables or
;; execute a setuptools based script. In such case, define a template:
;;
;; (dap-register-debug-template "My App"
;;   (list :type "python"
;;         :args "-i"
;;         :cwd nil
;;         :env '(("DEBUG" . "1"))
;;         :target-module (expand-file-name "~/src/myapp/.env/bin/myapp")
;;         :request "launch"
;;         :name "My App"))

;; ;;; ccls.el --- ccls client for lsp-mode
;; (use-package ccls-semantic-highlight
;;   :ensure ccls
;;   :commands ccls--publish-skipped-ranges ccls--publish-semantic-highlight)
;; (use-package ccls
;;   :ensure t
;;   :preface
;;   (defun my@lsp@stdio@connection@fun (&rest args) (cons ccls-executable ccls-args))
;;   :hook ((c-mode c++-mode objc-mode cuda-mode arduino-mode) . lsp)
;;   :config
;;   (add-to-list 'lsp-language-id-configuration '(arduino-mode . "arduino") t)
;;   (lsp-register-client
;;    (make-lsp--client
;;     :new-connection (lsp-stdio-connection #'my@lsp@stdio@connection@fun)
;;     :major-modes '(arduino-mode)
;;     :server-id 'ccls
;;     :multi-root nil
;;     :notification-handlers
;;     (lsp-ht ("$ccls/publishSkippedRanges" #'ccls--publish-skipped-ranges)
;;             ("$ccls/publishSemanticHighlight" #'ccls--publish-semantic-highlight))
;;     :initialization-options (lambda (&rest args) ccls-initialization-options)
;;     :library-folders-fn ccls-library-folders-fn)))

;;; lsp-clangd.el --- LSP clients for the C Languages Family
(use-package lsp-clangd
  :hook ((c-mode c++-mode objc-mode cuda-mode arduino-mode) . lsp)
  :custom
  (lsp-clangd-binary-path "/usr/bin/clangd")
  (lsp-clients-clangd-args '("--header-insertion-decorators=0" "--clang-tidy")))


(el-init-provide)



;;; init-lsp.el ends here
