;;; init-web.el --- Settings for web-mode

;; Copyright © 2014, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:39:55 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(require 'init-cerbere)
(require 'init-company)
(require 'init-misc) ;; for smartparens

;;; web-mode --- major mode for editing web templates
(use-package web-mode
  :ensure t
  :mode ((rx (or (: (or ".phtml" ".tpl.php" ".asp" ".gsp" ".jsp"
                        ".ascx" ".aspx" ".erb" ".mustache" ".djhtml") eos)
                 (: "template/" (+ any alnum) ".html"))))
  :init
  (setq web-mode-engines-alist
        '(("php"   . "\\.phtml\\'")
          ("blade" . "\\.blade\\."))))

;;; company-web --- Company version of ac-html, complete for web,
;;;                 html, emmet, jade, slim modes
(use-package company-web
  :ensure t
  :after company
  :functions company-web-html
  :preface
  (defun my@init@company@for@web@mode ()
    "Enable company for web-mode."
    (set (make-local-variable 'company-backends)
         '(company-web-html company-files))
    (company-mode t))
  :hook (web-mode . my@init@company@for@web@mode)
  :bind
  (:map web-mode-map
        ("C-'" . company-web-html)))

;;; smartparens-html --- Additional configuration for HTML based
;;;                      modes.
(use-package smartparens-html
  :ensure smartparens
  :bind
  (:map web-mode-map
        ("C-c C-f" . sp-html-next-tag)
        ("C-c C-b" . sp-html-previous-tag))
  )

;;; web-mode-edit-element --- Helper-functions for attribute- and
;;;                           element-handling
;; "web-mode-edit-element" is a smart enhancement for the package
;; web-mode inspired by the packages ParEdit and Paxedit.
;;
;; It provides a few helper-functions for attribute- and
;; element-handling based on the functions given by web-mode.
;; Further more it provides functions for slurping, barfing,
;; dissolving, raising ... elements inspired by ParEdit and Paxedit.
;; Last but not least this package includes a minor mode to provide
;; a keymap with default bindings using commands of web-mode and
;; this package.
;;
;; To use this package, add the following lines somewhere in you init
;; file:
(use-package web-mode-edit-element
  :ensure t
  :hook (web-mode . web-mode-edit-element-minor-mode))

(el-init-provide)



;;; init-web.el ends here
