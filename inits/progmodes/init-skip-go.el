;;; go-settings.el --- Settings for go-mode

;; Copyright © 2014, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 19:55:28 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(require 'init-spell)
(require 'init-cerbere)

;;; go-mode.el --- Major mode for the Go programming language
(use-package go-mode
  :ensure t
  :mode (rx ".go" eos))

;;; go-eldoc --- eldoc for go-mode
;; `go-eldoc.el' provides eldoc for Go language. `go-eldoc.el' shows
;; type information for variable, functions and current argument
;; position of function.
(use-package go-eldoc
  :ensure t
  :hook (go-mode . go-eldoc-setup))

;;; go-fill-struct --- Fill struct for golang.
;; For use this packages you should install `fillstruct' first:
;; % go get -u github.com/davidrjenni/reftools/cmd/fillstruct
(use-package go-fill-struct
  :ensure t
  :after go-mode
  :bind (:map go-mode-map
	      ("C-c f" . go-fill-struct)))

;;; go-capf --- Completion-at-point backend for go
;; Emacs built-in `completion-at-point' completion mechanism has no
;; support for go by default. This package helps solve the problem by
;; with a custom `completion-at-point' function, that should be added
;; to `completion-at-point-functions' as so:
;;
;;   (add-to-list 'completion-at-point-functions #'go-completion-at-point-function)
;;
;; Note that this requires gocode (https://github.com/mdempsky/gocode)
;; to be installed on your system, that's compatible with the version
;; of go you are using.
(use-package go-capf
  :ensure t
  :preface
  (defun my@go@mode@hook@fun (&rest args)
    "Ignore ARGS."
    (add-hook 'completion-at-point-functions #'go-capf nil t))
  :after go-mode
  :hook ((go-mode . my@go@mode@hook@fun)))

(el-init-provide)



;;; go-settings.el ends here
