;;; init-docker.el --- Various settings for accessing docker.

;; Copyright © 2019, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-06 16:36:01 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

;;; docker-compose-mode.el --- Major mode for editing docker-compose files
;; <https://github.com/meqif/docker-compose-mode>
;; Major mode for editing docker-compose files, providing
;; context-aware completion of docker-compose keys through
;; completion-at-point-functions.
;;
;; The completions can be used with the completion system shipped with
;; vanilla Emacs, and 3rd-party frontends like company-mode,
;; autocomplete, and ido-at-point.
(use-package docker-compose-mode
  :ensure t
  :commands docker-compose-mode)

;;; tramp-container.el --- Tramp integration for Docker-like containers
;; Allows Tramp access to environments provided by Docker and similar
;; programs.
;;
;; ## Usage
;;
;; Open a file on a running Docker container:
;;
;;     C-x C-f /docker:USER@CONTAINER:/path/to/file
;;
;; or Podman:
;;
;;     C-x C-f /podman:USER@CONTAINER:/path/to/file
;;
;; Where:
;;     USER          is the user on the container to connect as (optional)
;;     CONTAINER     is the container to connect to
;;
;;
;; Open file in a Kubernetes container:
;;
;;     C-x C-f /kubernetes:POD:/path/to/file
;;
;; Where:
;;     POD     is the pod to connect to.
;;             By default, the first container in that pod will be
;;             used.
;;
;; Completion for POD and accessing it operate in the current
;; namespace, use this command to change it:
;;
;; "kubectl config set-context --current --namespace=<name>"
(use-package tramp-container
  :ensure tramp
  :defer)

;;; docker.el --- Emacs interface to Docker
;; <https://github.com/Silex/docker.el>
;; This package allows you to manipulate docker images, containers &
;; more from Emacs.
(use-package docker
  :ensure t
  :bind (("C-c D" . docker)))

;;; dockerfile-mode.el --- Major mode for editing Docker's Dockerfiles
;; <https://github.com/spotify/dockerfile-mode>
;; Provides a major mode `dockerfile-mode' for use with the standard
;; `Dockerfile' file format.  Additional convenience functions allow
;; images to be built easily.
;;
;; Adds syntax highlighting as well as the ability to build the image
;; directly (C-c C-b) from the buffer.
;;
;; You can specify the image name in the file itself by adding a line
;; like this at the top of your Dockerfile.
;;
;;   ## -*- docker-image-name: "your-image-name-here" -*-
;;
;; If you don't, you'll be prompted for an image name each time you
;; build.
(use-package dockerfile-mode
  :ensure
  :mode ((rx "Dockerfile" eos)))

(el-init-provide)



;;; init-docker.el ends here
