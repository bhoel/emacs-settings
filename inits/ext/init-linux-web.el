;;; init-linux-web.el --- Settings for web access from emacs.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:53:38 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;; ;;; w3m.el --- an Emacs interface to w3m
;; ;; an Emacs interface to w3m
;; (use-package w3m
;;   :ensure t
;;   :if my-isux
;;   :no-require t
;;   :commands w3m)

;;; helm-w3m.el --- W3m bookmark - helm interface.
;; W3m bookmark - helm interface.
(use-package helm-w3m
  :after (helm)
  :ensure t
  :commands helm-w3m-bookmarks)

(el-init-provide)



;;; init-linux-web.el ends here
