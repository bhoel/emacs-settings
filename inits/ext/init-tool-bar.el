;;; init-tool-bar.el --- Settings for tool bar.

;; Copyright © 2014, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:35 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;;; tool-bar.el --- setting up the tool bar
;; Toggle the tool bar in all graphical frames (Tool Bar mode). With
;; a prefix argument ARG, enable Tool Bar mode if ARG is positive,
;; and disable it otherwise. If called from Lisp, enable Tool Bar
;; mode if ARG is omitted or nil.
(use-package tool-bar
  :config
  (tool-bar-mode -1))

(el-init-provide)



;;; init-tool-bar.el ends here
