;;; init-linux-gnus.el --- gnus startup file

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-06 18:18:21 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;; (setq debug-on-error t)
;; (setq imap-log t)

(require 'bind-key)

(require 'easymenu)

(use-package init-my-switches :load-path "inits")
(use-package init-epg :load-path "inits/ext")
(use-package init-path :load-path "inits/ext")

;;; mu4e-views.el --- View emails in mu4e using xwidget-webkit
(use-package mu4e-views :no-require t :ensure t)

;;; bbdb.el --- core of BBDB
(use-package bbdb
  :ensure t
  :commands bbdb-initialize
  :hook ((gnus-after-exiting-gnus . bbdb-save)
	 (mail-setup . bbdb-mail-aliases)
	 (message-setup . bbdb-mail-aliases))
  :custom
  ;; Default area code to use when reading a new phone number. This
  ;; variable also affects dialing.
  (bbdb-default-area-code 49)
  ;; If t BBDB will check its auto-save file. If this file is newer
  ;; than `bbdb-file', BBDB will offer to revert.
  (bbdb-check-auto-save-file t)
  ;; just remove some warning since bbdb package hook the mail-mode
  (compose-mail-user-agent-warnings nil)
  (bbdb-complete-mail-allow-cycling t))

;;; sendmail.el --- mail sending commands for Emacs
(use-package sendmail
  :commands (sendmail-query-once
             sendmail-user-agent-compose
             mail-mode mail
             mail-other-window
             mail-other-frame)
  :custom
  (mail-signature t)
  (send-mail-function #'smtpmail-send-it)
  (mail-default-reply-to (cond (my-ishome "berhoel@gmail.com")
			       (my-iswork "berthold.hoellmann@kumkeo.de"))))

(use-package smtpmail
  :if my-ishome
  :custom
  (smtpmail-smtp-server "localhost"))
(use-package smtpmail
  :if my-iswork
  :custom
  (mail-host-address "kumkeo.de")
  (smtpmail-smtp-server "smtphost.kumkeo.de"))

;;; message.el --- composing mail and news messages
(use-package message
  :functions message-insinuate-rmail message-cite-original-without-signature
  :custom
  (message-expires 8)
  (message-kill-buffer-on-exit t)
  (message-cite-function 'message-cite-original-without-signature)
  ;; message-insinuate-rmail
  ;; Adding (message-insinuate-rmail) and (setq mail-user-agent
  ;; 'gnus-user-agent) in .emacs convinces Rmail to compose, reply and
  ;; forward messages in message-mode, where you can enjoy the power
  ;; of MML.
  :config
  (message-insinuate-rmail))

;;; simple.el --- basic editing commands for Emacs
(use-package simple
  :custom
  (mail-user-agent 'gnus-user-agent))

;;; message-attachment-reminder.el --- Remind if missing attachment
;; Scans outgoing messages via `message-send-hook' to look for phrases
;; which indicate that an attachment is expected to be present but for
;; which no attachment has been inserted.
;;
;; The phrases to look for can be customized via
;; `message-attachment-reminder-regexp', whilst the warning message shown
;; to the user can be changed via
;; `message-attachment-reminder-warning'.
;;
;; Finally, by default quoted lines are not checked however this can be
;; changed by setting `message-attachment-reminder-exclude-quoted' to
;; `nil'.
(use-package message-attachment-reminder :ensure t)

;;; gnus-start.el --- startup functions for Gnus
(use-package gnus-start
  :custom
  (gnus-init-file (concat user-emacs-directory "emacs-gnus.el"))
  (gnus-check-new-newsgroups 'ask-server)
  (gnus-subscribe-newsgroup-method 'gnus-subscribe-topics)
  ;; Non-nil means that Gnus will read the entire active file at
  ;; startup. If this variable is nil, Gnus will only know about the
  ;; groups in your ‘.newsrc’ file.
  ;;
  ;; If this variable is ‘some’, Gnus will try to only read the
  ;; relevant parts of the active file from the server. Not all
  ;; servers support this, and it might be quite slow with other
  ;; servers, but this should generally be faster than both the t and
  ;; nil value.
  ;;
  ;; If you set this variable to nil or ‘some’, you probably still
  ;; want to be told about new newsgroups that arrive. To do that, set
  ;; ‘gnus-check-new-newsgroups’ to ‘ask-server’. This may not work
  ;; properly with all servers.
  (gnus-read-active-file t)
  ;; Non-nil means that Gnus will check and remove bogus newsgroup at
  ;; startup. If this variable is nil, then you have to tell Gnus
  ;; explicitly to check for bogus newsgroups with b.
  (gnus-check-bogus-newsgroups t))


;;; gnus.el --- a newsreader for GNU Emacs
(use-package gnus
  :commands gnus
  :hook ((message-mode . message-utils-setup)
	 (message-send . check-attachments-attached))
  :preface
  (defun message-utils-setup ()
    "Add menu-entries for message-utils."
    (easy-menu-add-item
     nil '("Message")
     ["Insert Region Marked..." message-mark-inserted-region t]
     "Insert File Marked...")
    (easy-menu-add-item
     nil '("Field")
     ["Crosspost / Followup" message-xpost-fup2 t] "----")
    (easy-menu-add-item
     nil '("Field")
     ["New Subject" message-change-subject t] "----")
    (easy-menu-add-item
     nil '("Field")
     ["Reduce To: to Cc:" message-reduce-to-to-cc t] "----")
    (easy-menu-add-item
     nil '("Field")
     [ "X-No-Archive:" message-add-archive-header t ]))
  ;; Here is another way to check for forgotten attachments. Taken from
  ;; <http://ww.telent.net/diary/2003/1/>
  (defun check-attachments-attached ()
    "Here is a way to check for forgotten attachments.
Taken from <http://ww.telent.net/diary/2003/1/>"
    (interactive)
    (save-excursion
      (goto-char (point-min))
      (let* (
	     ;; Nil when message came from outside (eg calling emacs
	     ;; as editor) Non-nil marker of end of headers.
	     (internal-messagep
	      (re-search-forward
	       (concat "^" (regexp-quote mail-header-separator) "$") nil t))
	     (end-of-headers ; Start of body.
	      (copy-marker
	       (or internal-messagep
		   (re-search-forward "^$" nil t)
		   (point-min))))
	     (limit
	      (or (re-search-forward "^-- $" nil t)
		  (point-max)))
	     (old-case-fold-search case-fold-search))
	(unwind-protect
	    (progn
	      (goto-char end-of-headers)
	      (when (re-search-forward "attach" limit t)
		(goto-char end-of-headers)
		;; the word 'attach' has been used, can we find an
		;; attachment?
		(unless
		    (or (re-search-forward "^<#/" limit t)
			(y-or-n-p
			 "No attachment. Send the message? ")
			(error "No message sent")))))
	  (set-marker end-of-headers nil)))))
  :custom
  (gnus-large-newsgroup nil)
  (gnus-spam-autodetect-methods
   '(("nntp:.*" . (spam-use-blacklist
		   spam-use-regex-headers
		   spam-use-bogofilter-headers
		   spam-use-bogofilter
		   spam-use-gmane-xref))))
  ;; ("nnml:.*" . (spam-use-blacklist
  ;;spam-use-regex-headers
  ;;spam-use-bogofilter-headers
  ;;spam-use-bogofilter
  ;;spam-use-gmane-xref))
  ;; ("nnimap:.*" . (spam-use-blacklist
  ;;spam-use-regex-headers
  ;;spam-use-bogofilter-headers
  ;;spam-use-bogofilter
  ;;spam-use-gmane-xref))
  (gnus-spam-autodetect '(("nntp:.*" . t)))
  ;; ("nnml:.*" . t)
  ;; ("nnimap:.*" . t)

  ;; Gnus provides a few different methods for storing the mail and
  ;; news you send. The default method is to use the archive virtual
  ;; server to store the messages. If you want to disable this
  ;; completely, the gnus-message-archive-group variable should be
  ;; nil, which is the default.
  ;;
  ;; gnus-message-archive-method says what virtual server Gnus is to
  ;; use to store sent messages.
  (gnus-message-archive-method
   '(nnml "archive"
	  (nnml-inhibit-expiry t)
	  (nnml-active-file "~/News/sent-mail/active")
	  (nnml-directory "~/News/sent-mail/")))
  ;; Documentation:
  ;; *Name of the group in which to save the messages you've written.
  ;; This can either be a string; a list of strings; or an alist of
  ;; regexps/functions/forms to be evaluated to return a string (or a
  ;; list of strings). The functions are called with the name of the
  ;; current group (or nil) as a parameter.
  ;;
  ;; If you want to save your mail in one group and the news articles
  ;; you write in another group, you could say something like:
  (gnus-message-archive-group
   '((if (message-news-p)
	 "sent-news"
       "sent-mail")))
  ;; (setq gnus-summary-line-format "%U%R%z%I%[%d %([%4L: %-20,20f%] %s%)\n")
  ;; http://my.gnus.org/node/view/121
  ;; "%U%R%z%I%(%[%4L: %-23,23f%]%) %s\n")
  (gnus-summary-line-format
   (concat
    "%5{%U%R%z"
    ;;"%ub"
    "%}%5k"
    "%4{|%}"
    "%2{%-16&user-date;%}"
    "%4{|%}"
    "%2{ %}%("
    ;;"%-24,24uB"
    "%-24,24a"
    "%4{|%}"
    ;; "%2{%6k %}"
    "%)"
    ;; "%4{|%}"
    "%2{ %}%*%3{%B %}%1{%s%}\n"))

  ;; (setq gnus-spam-newsgroup-contents
  ;;'(("^nnml:spam" gnus-group-spam-classification-spam)))
  ;; (setq gnus-use-cache t)

  :config
  (copy-face 'bold-italic '@my@time@)
  (set-face-foreground '@my@time@ "indianred4")

  (copy-face 'bold '@my@threads@)
  (set-face-foreground '@my@threads@ "indianred4")

  (copy-face 'bold '@my@grey@)
  (set-face-foreground '@my@grey@ "grey")

  (copy-face 'default '@my@black@)
  (set-face-foreground '@my@black@ "grey60")

  (copy-face 'default '@my@bigger@numbers@)
  (set-face-foreground '@my@bigger@numbers@ "indianred4")

  (setq gnus-face-2 '@my@time@)
  (setq gnus-face-3 '@my@threads@)
  (setq gnus-face-4 '@my@grey@)
  (defvar gnus-face-5 '@my@black@)
  (defvar gnus-face-6 '@my@bigger@numbers@)

  ;; Extract address components from a From header.
  ;; Given an RFC-822 (or later) address FROM, extract name and address.
  ;; Returns a list of the form (FULL-NAME CANONICAL-ADDRESS).  Much more simple
  ;; solution than ‘mail-extract-address-components’, which works much better, but
  ;; is slower.
  ;; (setq gnus-extract-address-components (quote mail-extract-address-components))

  ;; Initialize the Insidious Big Brother Database
  (bbdb-initialize 'gnus 'mail 'message 'anniv 'sc 'pgp)

  ;; A three pane layout, Group buffer on the left, summary buffer
  ;; top-right, article buffer bottom-right:
  (gnus-add-configuration
   '(article
     (horizontal 1.0
		 (vertical 50
			   (group 1.0))
		 (vertical 1.0
			   (summary 0.25 point)
			   (article 1.0)))))
  (gnus-add-configuration
   '(summary
     (horizontal 1.0
		 (vertical 50
			   (group 1.0))
		 (vertical 1.0
			   (summary 1.0 point)))))

  (cond
   ((member (downcase (system-name))
	    (list "pchoel" "pchoel.xn--hllmanns-n4a.de" "ubuntu-laptop"))
    (require 'my-gnus/system-home))
   ((member (downcase (system-name))
	    (list " - - - "))
    (require 'my-gnus/system-work))
   ((member (downcase (system-name))
	    (list
	     "kkcli937"))
    (require 'my-gnus/system-kumkeo))
   (t
    (error (format "Unknown system %s" (system-name)))))

;;; Anzeige von Nachrichten(Info-goto-node "(Gnus)Security")

  ;; Automatic decryption/verification of gpg/pgp parts

  ;; By default, you have to press “W s” or “W p” to decrypt/verify
  ;; pgp parts. Mutt does this automatically, and I think gnus
  ;; should do so, too. Here are my customisations to do that:

  ;; Tells Gnus how to display the part when it is requested
  (add-to-list 'mm-inline-media-tests
	       '("application/pgp$" mm-inline-text identity)))

;;; jka-compr.el --- reading/writing/loading compressed files
;; To use compression on gnus
(use-package jka-compr :after gnus)

;;; mml-sec.el --- A package with security functions for MML documents
(use-package mml-sec
  :after gnus
  :custom
  (mml-secure-openpgp-encrypt-to-self t)
  (mml-secure-openpgp-always-trust nil)
  (mml-secure-cache-passphrase nil)
  (mml-secure-passphrase-cache-expiry '36000)
  (mml-secure-openpgp-sign-with-sender t))

;;; mml1991.el --- Old PGP message format (RFC 1991) support for MML
(use-package mml1991 :after gnus)

;;; mml2015.el --- MIME Security with Pretty Good Privacy (PGP)
(use-package mml2015 :after gnus)

;;; mm-decode.el --- Functions for decoding MIME things
(use-package mm-decode
  :custom
  (mm-discouraged-alternatives '("text/html" "text/richtext"))
  ;; verify/decrypt only if mml knows about the protocl used
  (mm-verify-option 'known)
  (mm-decrypt-option 'known))

;;; nnheader.el --- header access macros for Gnus and its backends
(use-package nnheader
  :hook (nnmail-prepare-incoming . nnheader-ms-strip-cr))

;;; nnmail.el --- mail support functions for the Gnus mail backends
(use-package nnmail
  :hook ((nnmail-prepare-incoming-header . nnmail-remove-leading-whitespace)
	 (nnmail-prepare-incoming-header . nnmail-remove-list-identifiers)
	 (nnmail-prepare-incoming-header . nnmail-remove-tabs)
	 (nnmail-prepare-incoming-header . nnmail-ignore-broken-references))
  :custom
  (nnmail-crosspost nil)
  (nnmail-split-methods 'nnmail-split-fancy)
  (nnmail-use-long-file-names t)
  (nnmail-cache-accepted-message-ids t)
  (nnmail-crosspost nil)
  (nnmail-expiry-wait 7)
  (nnmail-keep-last-article nil)
  (nnmail-split-fancy-match-partial-words t)
  (nnmail-split-lowercase-expanded nil)
  (nnmail-treat-duplicates 'warn) ; or delete
  (nnmail-use-long-file-names t)
  (nnmail-list-identifiers
   (rx (or "[Ann]:"
	   "[Ann]"
	   "ANN :"
	   "ANN "
	   "ANN:"
	   "[ANN:]"
	   "[ANN]:"
	   "[ANN]"
	   "ANNOUNCE:"
	   "ANNOUNCE"
	   "[ANNOUNCE]"
	   "ANNOUNCEMENT:"
	   "[ANNOUNCEMENT]"
	   "Announcing"
	   "[Application]"
	   "[atlas-devel]"
	   "[biggles-announce]"
	   "[biggles-help]"
	   "BLITZ:"
	   "[bug-gsrc]"
	   "[Catalog-sig]"
	   "[Crew]"
	   "[CruiseControl]"
	   "[C++-sig]"
	   "CTAN has a new package:"
	   "CTAN package installation:"
	   "CTAN package update:"
	   "CTAN package upgrade:"
	   "CTAN submission ---"
	   "CTAN submission --"
	   "CTAN submission -"
	   "CTAN submission:"
	   "CTAN update --"
	   "CTAN update:"
	   "CTAN Update:"
	   "CTAN upload --"
	   "CTAN upload:"
	   "CTAN Upload ---"
	   "CTAN Upload --"
	   "CTAN Upload -"
	   "CTAN upload notification:"
	   "New on CTAN:"
	   "[dante-ev]"
	   "[DB-SIG]"
	   "[devpi-dev]"
	   "[Distutils]"
	   "DOCBOOK:"
	   "[Doc-SIG]"
	   "[Docutils-users]"
	   "[DO-SIG]"
	   "[Doxygen-announce]"
	   "[ECB-list]"
	   "[eGenix.com]"
	   "[eGenix.com]"
	   "[egenix-users]"
	   "[f2py]"
	   "[fds-smv]"
	   "[fmII]"
	   "[fnorb]"
	   "[fnorb-dev]"
	   "[Fontinst]"
	   "[FSF]"
	   "[fwrap-users]"
	   "[getopt-sig]"
	   "[Gnuplot-beta]"
	   "[Gnuplot-py-users]"
	   "[gts-announce]"
	   "[help-gengetopt]"
	   "[Help-gsl]"
	   "[hh-pythoneers:[0-9]+]"
	   "[Image-SIG]"
	   "[Import-sig]"
	   "[innosetup-announce]"
	   "[JIRA]"
	   "[l2h]"
	   "[LC Announce]"
	   "[LC Interest]"
	   "[LC Interest] [New Linux Counter Project]"
	   "[leafnode-list]"
	   "[LiCo]"
	   "[Math-atlas-announce]"
	   "[Math-atlas-devel]"
	   "[Math-atlas-errors]"
	   "[Matplotlib-announce]"
	   "[Matplotlib-users]"
	   "[metafont]"
	   "[Metapost]"
	   "[Module]"
	   "[mpatrol]"
	   "New CTAN package:"
	   "[Numpy-discussion]"
	   "OON:"
	   "[oon-list]"
	   "[opensuse-announce]"
	   "[optik]"
	   "[orm-devel]"
	   "[orm-users]"
	   "[OSDN Events]"
	   "[Package]"
	   "[Persistence-sig]"
	   "[plasTeX]"
	   "[plucker-announce]"
	   "[Podofo-users]"
	   "[Psh]"
	   "[Psh-Heimrat]"
	   "[MRH]"
	   "[MRH-Heimrat]"
	   "[opensuse-factory]"
	   "[poky]"
	   "[pygtk]"
	   "[PyInstaller]"
	   "[PyOpenGL]"
	   "[Pyopengl-users]"
	   "[Pytables-announce]"
	   "[Pytables-users]"
	   "[Python-de]"
	   "Re: CTAN submission ---"
	   "Re: CTAN submission --"
	   "Re: CTAN submission -"
	   "Re: CTAN submission:"
	   "Re: CTAN Upload ---"
	   "Re: CTAN Upload --"
	   "Re: CTAN Upload -"
	   "Re: dante.ctan.org upload:"
	   "[Rekall]"
	   "[Rekall-announce]"
	   "RELEASE:"
	   "RELEASED -"
	   "RELEASED: "
	   "RELEASED:"
	   "[Roundup-users]"
	   "[sane-devel]"
	   "[SciPy-chaco]"
	   "[SciPy-dev]"
	   "[SciPy-user]"
	   "[SLApple]"
	   "[suse-security-announce]"
	   "[Swig]"
	   "[TEX-D-L]"
	   "[tex-fonts]"
	   "[Types-sig]"
	   "[ulb-announce]"
	   "[Utility]"
	   "[Whmpsh]"
	   "[Wrapper]"
	   "[wxPython]"
	   "[Xansys]"
	   "[xfoil]"
	   "[Ximian Updates]"
	   "[Xml-logilab]"
	   "[XML-Projects]"
	   "[XML-SIG]"
	   "[yocto]"
	   "[Zope]"
	   "[Zope-Annce]"
	   "[zope-de]"
	   "[Zope_de]"
	   "[Zope.de]"))))

;;; boxquote.el --- Quote text with a semi-box.
(use-package boxquote
  :ensure t
  :bind (
	 :map message-mode-map
	 ("C-c b i" . boxquote-insert-file)
	 ("C-c b k" . boxquote-describe-key)
	 ("C-c b f" . boxquote-describe-function)
	 ("C-c b v" . boxquote-describe-variable)
	 ("C-c b p" . boxquote-paragraph)
	 ("C-c b r" . boxquote-region)
	 ("C-c b t" . boxquote-title)
	 ("C-c b u" . boxquote-unbox)
	 ("C-c b y" . boxquote-yank))
  :hook (message-mode . rs-boxquote-menu-setup)
  :preface
  (defun rs-boxquote-menu-setup ()
    "Setup boxquote functions."
    (easy-menu-add-item
     nil '("Message")
     '("Boxquote"
       ["Insert File" boxquote-insert-file t]
       ["Describe Key" boxquote-describe-key t]
       ["Describe Function" boxquote-describe-function t]
       ["Describe Variable" boxquote-describe-variable t]
       ["Paragraph" boxquote-paragraph t]
       ["Region" boxquote-region t]
       ["Title" boxquote-title t]
       ["Unbox" boxquote-unbox t]
       ["Yank" boxquote-yank t])
     "Spellcheck")))
;; Zum boxquoten der aktuellen Markierung muss man nun zum Beispiel
;; C-c b r eingeben, der aktuelle Abschnitt wird mit C-c b p
;; geboxquotet und mit C-c b t fügt man einen Titel ein.

;; removed from MELPA
;; ;;; message-x.el -- customizable completion in message headers
;; (use-package message-x :ensure t)

;;; gnus-cite.el --- parse citations in articles for Gnus
(use-package gnus-cite
  :hook (gnus-article-display . gnus-article-highlight-citation))

;;; gnus-topic.el --- a folding minor mode for Gnus group buffers
(use-package gnus-topic
  :hook (gnus-group-mode . gnus-topic-mode)
  :custom
  ;; If non-nil, display the topic lines even of topics that have no
  ;; unread articles.
  (gnus-topic-display-empty-topics nil))

;;; gnus-registry.el --- article registry for Gnus
;; This is the gnus-registry.el package, which works with all Gnus
;; backends, not just nnmail. The major issue is that it doesn't go
;; across backends, so for instance if an article is in nnml:sys and
;; you see a reference to it in nnimap splitting, the article will
;; end up in nnimap:sys
(use-package gnus-registry
  :disabled
  :hook (gnus-started . gnus-registry-initialize)
  :custom
  (gnus-registry-max-entries 25000)
  (gnus-registry-track-extra '(sender subject recipient))
  (gnus-registry-cache-file
   (concat user-emacs-local-directory ".gnus.registry.eieio")))

;; ;;; all-the-icons-gnus --- Shows icons for in Gnus
;; (use-package all-the-icons-gnus
;;   :ensure t
;;   :after gnus
;;   :config
;;   (all-the-icons-gnus-setup))

;;; canlock.el --- functions for Cancel-Lock feature
(use-package canlock
  :hook (message-header . canlock-insert-header)
  :config
  (load-library (concat user-emacs-directory "passwords.el.gpg")))

;; ;; Desktop notification integration in Gnus!? Ohh goody!
;; ;;
;; ;; ``gnus-desktop-notify.el`` provides a simple mechanism to notify the user
;; ;; when new messages are received. For basic usage, to be used in conjunction
;; ;; with ``gnus-daemon``, put the following:
;; (require 'gnus-desktop-notify)
;; (gnus-desktop-notify-mode)
;; (gnus-demon-add-scanmail)

;;; gnus-art.el --- article mode commands for Gnus
(use-package gnus-art
  :hook (gnus-article-display . gnus-article-emphasize)
  :custom
  (gnus-treat-body-boundary 'head)
  (gnus-article-skip-boring t)
  (gnus-article-mode-line-format "Gnus: %g %S%m")
  (gnus-treat-x-pgp-sig t)
  ;; Security-Buttons anzeigen
  (gnus-buttonized-mime-types
   '("multipart/alternative"
     "multipart/encrypted"
     "multipart/signed"))
  (gnus-visible-headers
   (rx bol
       (or "From"
	   "Newsgroups"
	   "Subject"
	   "Date"
	   "Followup-To"
	   "Reply-To"
	   "Organization"
	   "Summary"
	   "Keywords"
	   "To"
	   "Cc"
	   "BCc"
	   "GCc"
	   "FCc"
	   "Posted-To"
	   "Mail-Copies-To"
	   "Mail-Followup-To"
	   "Apparently-To"
	   "Gnus-Warning"
	   "Resent-From"
	   "X-Sent"
	   "Authentication-Results") ":"))
  (gnus-treat-display-smileys t)
  :config
  ;; X-Request-PGP-Header anklickbar machen
  (add-to-list
   'gnus-header-button-alist
   '("^X-Request-PGP:" gnus-button-url-regexp
     0 (>= gnus-button-browse-level 0) gnus-button-openpgp 0)
   t))

;;; smiley.el --- displaying smiley faces  -*- lexical-binding: t; -*-
;; A re-written, simplified version of Wes Hardaker's XEmacs smiley.el
;; which might be merged back to smiley.el if we get an assignment for
;; that.  We don't have assignments for the images smiley.el uses, but
;; I'm not sure we need that degree of rococoness and defaults like a
;; yellow background.  Also, using PBM means we can display the images
;; more generally.  -- fx
(use-package smiley
  :custom
  (smiley-style 'emoji))

;;; gnus-async.el --- asynchronous support for Gnus
(use-package gnus-async
  :after gnus
  :custom
  (gnus-asynchronous t)
  (gnus-use-header-prefetch t))

;;; gnus-msg.el --- mail and post interface for Gnus
(use-package gnus-msg
  :after gnus-startup-hook
  :custom
  (gnus-message-replysign t)
  (gnus-confirm-mail-reply-to-news t))

;;; gnus-score.el --- scoring code for Gnus
(use-package gnus-score
  :after gnus
  :custom
  ;; Function used to find score files.
  (gnus-score-find-score-files-function '(gnus-score-find-bnews bbdb/gnus-score)))

;;; gnus-sum.el --- summary mode commands for Gnus
(use-package gnus-sum
  :after (:all gnus nnmail)
  :custom
  (gnus-thread-indent-level 2)
  (gnus-fetch-old-headers 'some)
  (gnus-list-identifiers nnmail-list-identifiers)
  (gnus-summary-display-while-building 30)
  (gnus-summary-expunge-below -999)
  (gnus-summary-make-false-root 'adopt)
  (gnus-thread-ignore-subject t)
  ;; *================================
  ;; * Formatting the summary buffer: sorting by thread
  ;; * gnus-thread-sort-by-author      - Sort threads by root author.
  ;; * gnus-thread-sort-by-chars- Sort threads by root article octet length.
  ;; * gnus-thread-sort-by-date- Sort threads by root article date.
  ;; * gnus-thread-sort-by-lines- Sort threads by root article Lines header.
  ;; * gnus-thread-sort-by-number      - Sort threads by root article number.
  ;; * gnus-thread-sort-by-score- Sort threads by root article score.
  ;; * gnus-thread-sort-by-subject     - Sort threads by root subject.
  ;; * gnus-thread-sort-by-total-score - Sort threads by the sum of all scores in the thread.
  ;; *================================
  (gnus-thread-sort-functions '(gnus-thread-sort-by-number))
  ;; gnus-thread-sort-by-date
  ;; gnus-thread-sort-by-subject
  ;; gnus-thread-sort-by-author
  ;; gnus-thread-sort-by-score
  ;; gnus-thread-sort-by-total-score
  (gnus-summary-thread-gathering-function 'gnus-gather-threads-by-references)
  ;; Von Modelines und ähnlichen Schweinereien
  (gnus-summary-mode-line-format "%g [%S] %Z")
  (gnus-summary-same-subject "")
  (gnus-sum-thread-tree-false-root      (if window-system "◯" "> "))
  (gnus-sum-thread-tree-single-indent   (if window-system "◎" ""))
  (gnus-sum-thread-tree-root            (if window-system "┌▶" "> "))
  (gnus-sum-thread-tree-vertical        (if window-system "│" "| "))
  (gnus-sum-thread-tree-leaf-with-other (if window-system "├┬▶" "+-> "))
  (gnus-sum-thread-tree-single-leaf     (if window-system "╰─▶" "\\-> "))
  (gnus-sum-thread-tree-indent          (if window-system "  " "  "))
  ;; If non-nil, fill in the gaps in threads.
  ;; If ‘some’, only fill in the gaps that are needed to tie loose
  ;; threads together. If ‘more’, fill in all leaf nodes that Gnus can
  ;; find. If non-nil and non-‘some’, fill in all gaps that Gnus
  ;; manages to guess.
  (gnus-build-sparse-threads 'more))

(use-package gnus-search
  :after gnus
  :custom
  (gnus-search-use-parsed-queries t))

;;; bbdb-gnus-aux.el --- aux parts of BBDB interface to Gnus
(use-package bbdb-gnus-aux
  :ensure bbdb
  :after bbdb
  :custom
  ;; If this is set, then every mail address in the BBDB that does not have
  ;; an associated score field will be assigned this score.  A value of nil
  ;; implies a default score of zero.
  (bbdb/gnus-score-default 5))

;;; bbdb-mua.el --- various MUA functionality for BBDB
(use-package bbdb-mua
  :ensure bbdb
  :after bbdb
  :custom
  ;; Default mark for message addresses known to BBDB. If nil do not
  ;; mark message addresses known to BBDB. See
  ;; ‘bbdb-mua-summary-mark’ and ‘bbdb-mua-summary-unify’. See also
  ;; ‘bbdb-mua-summary-mark-field’.
  (bbdb-mua-summary-mark (if window-system "▷" "+")))

;; ;;; bbdb- --- provide interface for more easily search/choice than BBDB.
;; ;; Make config suit for you. About the config item, eval the following
;; ;; sexp. (customize-group "bbdb-")
;; ;; Do setup
;; (use-package bbdb-
;;   :after bbdb
;;   :ensure t
;;   :functions bbdb-:setup bbdb-:start-completion
;;   :config
;;   (bbdb-:setup))

;;; bbdb-vcard --- vCard import/export for BBDB
(use-package bbdb-vcard
  :ensure t
  :after bbdb)

;;; gmail2bbdb --- import email and name into bbdb from vcard.
(use-package gmail2bbdb
  :ensure t
  :commands gmail2bbdb-import-file
  :init
  ;; import Gmail contacts in vcard format into bbdb
  (setq gmail2bbdb-bbdb-file (concat user-emacs-directory "bbdb_gmail_import")))

;;; helm-bbdb --- Helm interface for bbdb
(use-package helm-bbdb
  :after (:all helm bbdb)
  :ensure t)

;;; spam.el --- Identifying spam
(use-package spam
  :disabled
  :hook (gnus-startup . spam-initialize)
  :custom
  (spam-use-bogofilter t)
  (spam-use-regex-headers nil))

;;; spam-report.el --- Reporting spam
(use-package spam-report
  :after gnus
  :demand t
  :commands spam-report-url-ping-mm-url
  :custom
  (spam-report-gmane-use-article-number nil)
  (spam-report-url-ping-function #'spam-report-url-ping-mm-url))

(el-init-provide)



;;; init-linux-gnus.el ends here
