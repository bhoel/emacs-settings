;;; init-projectile.el --- Settings for `projectile'.

;; Copyright © 2014, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:49 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

;;; projectile.el --- Manage and navigate projects in Emacs easily
;; Projectile is a project interaction library for Emacs. Its goal is
;; to provide a nice set of features operating on a project level
;; without introducing external dependencies(when feasible). For
;; instance - finding project files has a portable implementation
;; written in pure Emacs Lisp without the use of GNU find (but for
;; performance sake an indexing mechanism backed by external commands
;; exists as well).
;;
;; Projectile tries to be practical - portability is great, but if
;; some external tools could speed up some task substantially and the
;; tools are available, Projectile will leverage them.
;;
;; By default, git, mercurial, darcs and bazaar are considered
;; projects. So are lein, maven, sbt, scons, rebar and bundler. If you
;; want to mark a folder manually as a project just create an empty
;; .projectile file in it.
(use-package projectile
  :ensure t
  :demand t
  :commands (projectile-mode
             projectile-project-p
             projectile-project-root
             projectile-current-project-dirs
             projectile-file-cached-p)
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :custom
  (projectile-completion-system 'helm)
  ;; Using Emacs Lisp for indexing files is really slow on Windows. To
  ;; enable external indexing, add this setting:
  ;; The alien indexing method uses external tools (e.g. git, find,
  ;; etc) to speed up the indexing process.
  (projectile-indexing-method (if (executable-find "alien") 'alien 'hybrid))
  (projectile-file-exists-local-cache-expire 600)
  (projectile-file-exists-remote-cache-expire 600)
  :config
  (setq projectile-file-exists-cache-timer 600)
  (projectile-mode))

;;; helm-projectile.el --- Helm integration for Projectile
(use-package helm-projectile
  :ensure t
  :after (helm-projectile)
  :commands helm-projectile-on
  :config
  (helm-projectile-on))

(el-init-provide)



;;; init-projectile.el ends here
