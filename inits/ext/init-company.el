;;; init-company.el --- Settings for `company'. -*- lexical-binding: t -*-

;; Copyright © 2014, 2022 by Berthold Höllmann

;; Time-stamp: <2023-08-04 20:45:16 hoel>

;; Author: Berthold Höllmann <berhoel@gmail.com>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(require 'init-my-switches)
(when my-isux
  (require 'init-linux-irony))

;;; company - Modular in-buffer completion framework for Emacs
(use-package company
  :ensure t
  :preface
  (defun my@org@mode@hook@fun ()
    (setq-local company-backends
		'((company-yasnippet company-dabbrev))))
  :defines company-mode-map company-begin-commands
  :hook ((after-init . global-company-mode)
	 (org-mode . my@org@mode@hook@fun)
	 (racer-mode . company-mode))
  :custom
  (company-idle-delay 0.0)
  (company-minimum-prefix-length 1))

;;; company-math.el --- Completion back-ends for for math unicode symbols and latex tags
;; global activation of the unicode symbol completion
(use-package company-math
  :ensure t
  :after company
  :functions company-math-symbols-unicode
  :config
  (add-to-list 'company-backends #'company-math-symbols-unicode))

;;; company-c-headers --- mode backend for C/C++ header files
;; company-c-headers provides auto-completion for C/C++ headers
;; using Company. After installing from MELPA, set it up:
(use-package company-c-headers
  :ensure t
  :after company
  :functions company-c-headers
  :config
  (add-to-list 'company-backends #'company-c-headers))

;;; company-irony --- company-mode completion back-end for irony-mode
;; This package provides a company-mode asynchronous completion
;; backend for the C, C++ and Objective-C languages.
(use-package company-irony
  :ensure t
  :disabled t
  :if my-isux
  :after company
  :functions company-irony
  :hook (irony-mode . company-irony-setup-begin-commands)
  :config
  (add-to-list 'company-backends #'company-irony))

;;; company-irony-c-headers --- Company mode backend for C/C++ header files with Irony
;; Homepage: https://github.com/hotpxl/company-irony-c-headers
;;
;; This file provides `company-irony-c-headers`, a company backend
;; that completes C/C++ header files. Large chunks of code are taken
;; from
;; [company-c-headers](https://github.com/randomphrase/company-c-headers).
;; It also works with `irony-mode` to obtain compiler options.
;;
;; When compiler options change, call
;; `company-irony-c-headers-reload-compiler-output` manually to
;; reload.
(use-package company-irony-c-headers
  :ensure t
  :disabled t
  :if my-isux
  :after company
  :functions company-irony-c-headers
  :config
  (add-to-list 'company-backends #'company-irony-c-headers))

;;; company-restclient --- company-mode completion back-end for
;;;                        restclient-mode
;; It provides auto-completion for HTTP methods and headers in
;; restclient-mode. Completion source is given by
;; know-your-http-well.
(use-package company-restclient
  :ensure t
  :after company
  :functions company-restclient
  :config
  (add-to-list 'company-backends #'company-restclient))

;;; company-statistics --- Sort candidates using completion history
;; Company-statistics is a global minor mode built on top of the
;; in-buffer completion system company-mode. The idea is to keep a
;; log of a certain number of completions you choose, along with
;; some context information, and use that to rank candidates the
;; next time you have to choose — hopefully showing you likelier
;; candidates at the top of the list.
(use-package company-statistics
  :ensure t
  :if my-isux
  :hook (company-mode . company-statistics-mode))

;;; helm-company --- Helm interface for company-mode
(use-package helm-company
  :ensure t
  :after (:all helm company)
  :bind (
         :map company-mode-map
         ("C-:" . helm-company)
         :map company-active-map
         ("C-:" . helm-company)))

;;; company-quickhelp --- Popup documentation for completion
;;;                       candidates
;; One of the things I missed the most when moving from
;; auto-complete to company was the documentation popups that would
;; appear when idling on a completion candidate. This package
;; remedies that situation.
;;
;; auto-complete uses popup-el to do it's thing and this results in
;; quite a few glitches. This package uses the much better pos-tip
;; to display the popups. I recommend installing pos-tip using melpa
;; which fetches the version of pos-tip which is located here. This
;; version contains a few bugfixes not included in the original on
;; emacswiki.
;;(use-package company-quickhelp
;;  :ensure t
;;  :after company
;;  :functions company-quickhelp-mode
;;  :config
;;  (company-quickhelp-mode))

;;; company-files.el --- company-mode completion back-end for file
;;; paths
(use-package company-files
  :ensure company
  :after company
  :commands company-files)

(el-init-provide)



;;; init-company.el ends here
