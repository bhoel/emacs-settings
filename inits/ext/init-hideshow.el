;;; init-hideshow.el --- Settings for hideshow.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 19:55:34 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

;;; hideshow.el --- minor mode cmds to selectively display
;;;                 code/comment blocks
(use-package hideshow
  ;; optional key bindings, easier than hs defaults
  :bind ([S-f7] . hs-toggle-hiding)
  :hook ((nxml-mode . hs-minor-mode)
         (c-mode-common . hs-minor-mode)
         (emacs-lisp-mode . hs-minor-mode)
         ;; (bibtex-mode . hs-minor-mode)
         (java-mode . hs-minor-mode)
         (lisp-mode . hs-minor-mode)
         (perl-mode . hs-minor-mode)
         (sh-mode . hs-minor-mode)
         (fortran-mode . hs-minor-mode)
         (python-mode . hs-minor-mode)))

(el-init-provide)



;;; init-hideshow.el ends here
