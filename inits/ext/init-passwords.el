;;; init-passwords.el --- Organize passwords from emacs.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:53:37 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;;; pass.el --- Major mode for password-store.el
(use-package pass
  :ensure t
  :commands pass)

;;; password-generator.el --- Password generator for humans. Good,
;;;                           Bad, Phonetic passwords included.
(use-package password-generator
  :ensure t
  :commands (password-generator-numeric
             ;; generate PIN-code or any other numeric password.
             password-generator-simple
             ;; simple password for most websites.
             password-generator-phonetic
             ;; easy to remember password.
             password-generator-strong
             ;; strong password and still suitable for most web sites
             ;; with strange password requirements to used special
             ;; chars.
             password-generator-paranoid))

(el-init-provide)



;;; init-passwords.el ends here
