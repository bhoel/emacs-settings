;;; init-rst.el --- Emacs settings for ReST and Sphinx

;; Copyright © 2009, 2014, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:48 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'init-autoinsert)

(use-package init-autoinsert
  :load-path "inits/ext"
  :functions my-yas-expand-by-uuid)

;;; rst.el --- Mode for viewing and editing reStructuredText-documents.
(use-package rst
  :mode ("\\.\\(rst\\\\|rest\\)\\'" . rst-mode))

;;; poly-rst.el --- poly-rst-mode polymode
;; Polymode for working with files using ReStructured Text (ReST or rst) markup
;; syntax.  This allows the user to work in regions marked with code using the
;; appropriate Emacs major mode.
(use-package poly-rst
  :ensure t
  :commands poly-rst-mode)

(defun my@insert@rst@head ()
  "Insert yas head for .rst files."
  (my-yas-expand-by-uuid 'rst-mode "head"))
(define-auto-insert (rx ".rst" eos) #'my@insert@rst@head)

(el-init-provide)



;;; init-rst.el ends here
