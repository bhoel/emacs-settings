;;; init-calendar.el --- Settings for `calendar.el'.

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2024-05-02 20:43:58 hoel>

;;; Commentary:

;;; Code:

;; Modifications for calendar are done here:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;;; calendar.el --- calendar functions
(use-package calendar
  :commands calendar calendar-set-date-style
  :hook ((today-visible-calendar . calendar-mark-today))
  :custom
  (calendar-abbrev-length 2)
  (calendar-christian-all-holidays-flag t)
  (calendar-date-style 'iso)
  (calendar-day-name-array
   ["Sonntag" "Montag" "Dienstag" "Mittwoch"
    "Donnerstag" "Freitag" "Samstag"])
  (calendar-month-name-array
   ["Januar" "Februar" "März" "April" "Mai" "Juni"
    "Juli" "August" "September" "Oktober" "November" "Dezember"])
  (calendar-mark-diary-entries-flag t)
  (calendar-mark-holidays-flag t)
  (calendar-view-holidays-initially-flag t)
  (calendar-week-start-day 1)
  :config
  (calendar-set-date-style 'european))

;;; solar.el --- calendar functions for solar events
;; See calendar.el.  This file implements features that deal with
;; times of day, sunrise/sunset, and equinoxes/solstices.
;;
;; Based on the ``Almanac for Computers 1984,'' prepared by the Nautical
;; Almanac Office, United States Naval Observatory, Washington, 1984, on
;; ``Astronomical Formulae for Calculators,'' 3rd ed., by Jean Meeus,
;; Willmann-Bell, Inc., 1985, on ``Astronomical Algorithms'' by Jean Meeus,
;; Willmann-Bell, Inc., 1991, and on ``Planetary Programs and Tables from
;; -4000 to +2800'' by Pierre Bretagnon and Jean-Louis Simon, Willmann-Bell,
;; Inc., 1986.
(use-package solar
  :after calendar
  :defines calendar-time-display-form
  :custom
  (calendar-latitude [53 37 north])
  (calendar-longitude [10 08 east])
  (calendar-location-name "Hamburg, Germany")
  (calendar-time-display-form
   '(24-hours ":" minutes (and time-zone (concat " (" time-zone ")")))))

;;; cal-tex.el --- calendar functions for printing calendars with
;;;                LaTeX
(use-package cal-tex
  :after calendar
  :custom
  (cal-tex-holidays t)
  (cal-tex-diary t))


;;; german-holidays --- German holidays for Emacs calendar
(use-package german-holidays
  :ensure t
  :after calendar)

;;; calfw --- Calendar view framework on Emacs
(use-package calfw
  :ensure t
  :after calendar)

;;; calfw-gcal --- edit Google calendar for calfw.el.
(use-package calfw-gcal
  :ensure t
  :after calfw)

;;; solar.el --- calendar functions for solar events
(use-package solar
  :custom
  (solar-n-hemi-seasons
   '("Frühlingsanfang" "Sommeranfang" "Herbstanfang" "Winteranfang")))

;;; holidays.el --- holiday functions for the calendar package
(use-package holidays
  :after german-holidays
  :commands calendar-mark-holidays
  :custom
  (holiday-general-holidays
   '((holiday-fixed 1 1 "Neujahr")
     (holiday-fixed 5 1 "1. Mai")
     (holiday-fixed 10 3 "Tag der Deutschen Einheit")))

  ;; Feiertage für Hamburg, weitere auskommentiert
  (holiday-christian-holidays
   '((holiday-float 12 0 -4 "1. Advent" 24)
     (holiday-float 12 0 -3 "2. Advent" 24)
     (holiday-float 12 0 -2 "3. Advent" 24)
     (holiday-float 12 0 -1 "4. Advent" 24)
     (holiday-fixed 12 25 "1. Weihnachtstag")
     (holiday-fixed 12 26 "2. Weihnachtstag")
     ;; (holiday-fixed 1 6 "Heilige Drei Könige")
     ;; (holiday-easter-etc -48 "Rosenmontag")
     ;; (holiday-easter-etc -3 "Gründonnerstag")
     (holiday-easter-etc  -2 "Karfreitag")
     (holiday-easter-etc   0 "Ostersonntag")
     (holiday-easter-etc  +1 "Ostermontag")
     (holiday-easter-etc +39 "Christi Himmelfahrt")
     (holiday-easter-etc +49 "Pfingstsonntag")
     (holiday-easter-etc +50 "Pfingstmontag")
     ;; (holiday-easter-etc +60 "Fronleichnam")
     ;; (holiday-fixed 8 15 "Mariae Himmelfahrt")
     (holiday-fixed 10 31 "Reformationstag")
     ;; (holiday-fixed 8 15 "Mariae Himmelfahrt")
     ;; (holiday-fixed 11 1 "Allerheiligen")
     ;; (holiday-float 11 3 1 "Buss- und Bettag" 16)
     (holiday-float 11 0 1 "Totensonntag" 20)))
  (holiday-local-holidays holiday-german-holidays)
  (calendar-holidays
   (append holiday-local-holidays
	   holiday-other-holidays
	   holiday-christian-holidays
	   holiday-solar-holidays)))

;;; diary-lib.el --- diary functions
(use-package diary-lib
  :commands diary-mark-entries diary-show-all-entries
  :hook (after-init . diary-show-all-entries)
  :custom
  (diary-number-of-entries 5)
  (diary-file (concat user-emacs-directory "diary"))
  :config
  (if (not calendar-mark-diary-entries-flag)
      (progn
        (diary-mark-entries)
        (calendar-mark-holidays))))

(el-init-provide)



;;; init-calendar.el ends here
