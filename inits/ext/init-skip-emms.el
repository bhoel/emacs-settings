;;; init-emms.el --- Setting up for `emms'.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:40:03 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;;; emms-playlist-mode.el --- Playlist mode for Emms.
(use-package emms-playlist-mode
  :ensure emms
  :commands emms
  :config)

;;; emms-setup.el --- Setup script for EMMS
(use-package emms-setup
  :after emms
  :functions emms-default-players emms-all
  :custom
  (emms-source-file-default-directory
   (expand-file-name "~/Musik/Musik/")))

;;; emms-browser.el --- a track browser supporting covers and
;;;                     filtering
(use-package emms-browser)

;;; emms-info-mediainfo.el --- Info-method for EMMS using medianfo
(use-package emms-info-mediainfo
  :ensure t
  :after emms
  :functions emms-info-mediainfo
  :custom
  (emms-info-functions (append emms-info-functions '(emms-info-mediainfo))))

;;; emms-mark-ext.el --- Extra functions for emms-mark-mode and
;;;                      emms-tag-edit-mode
(use-package emms-mark-ext
  :ensure t
  :after emms)

;; ;;; helm-emms.el --- Emms for Helm.
;; (use-package helm-emms
;;   :after (:all emms helm)
;;   :ensure t
;;   :custom
;;   (helm-emms-use-track-description-function nil))

(emms-default-players)
(emms-all)

(el-init-provide)



;;; init-emms.el ends here
