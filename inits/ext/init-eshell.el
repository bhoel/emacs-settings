;;; init-eshell.el --- Setup for `eshell-mode'.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:54 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(use-package init-my-switches :load-path "inits")

;;; eshell.el --- the Emacs command shell
(use-package eshell
  :commands eshell)

(use-package esh-mode
  :after eshell
  :defines eshell-mode-map)

;;; eshell-did-you-mean --- command not found (“did you mean…”
;;;                         feature) in Eshell.
(use-package eshell-did-you-mean
  :ensure t
  :demand
  :after esh-mode
  :commands eshell-did-you-mean-output-filter
  :custom
  (eshell-preoutput-filter-functions
   (append eshell-preoutput-filter-functions
	  '(eshell-did-you-mean-output-filter))))

;;; eshell-fringe-status.el --- Show last status in fringe
;; Show an indicator of the status of the last command run in eshell.
;; To use, enable `eshell-fringe-status-mode' in `eshell-mode'. The
;; easiest way to do this is by adding a hook:
(use-package eshell-fringe-status
  :ensure t
  :hook ((eshell-mode . eshell-fringe-status-mode)))

;;; pcomplete.el --- programmable completion
;; This module provides a programmable completion facility using
;; "completion functions". Each completion function is responsible for
;; producing a list of possible completions relevant to the current
;; argument position.
;;
;; To use pcomplete with shell-mode, for example, you will need the
;; following in your init file:
(use-package pcomplete
  :hook ((shell-mode . pcomplete-shell-setup)))

;;; pcmpl-args --- Enhanced shell command completion
;; This package extends option and argument completion of shell
;; commands read by Emacs. It is intended to make shell completion
;; in Emacs comparable to the rather excellent completion provided
;; by both Bash and Zsh.
;;
;; This package uses `pcomplete' to define completion handlers which
;; are used whenever shell completion is performed. This includes
;; when commands are read in the minibuffer via `shell-command'
;; (M-!) or in `shell-mode'.
;;
;; Completion support is provided for many different commands
;; including:
;;
;;   - GNU core utilities (ls, rm, mv, date, sort, cut, printf, ...)
;;
;;   - Built-in shell commands (if, test, time, ...)
;;
;;   - Various GNU/Linux commands (find, xargs, grep, man, tar, ...)
;;
;;   - Version control systems (bzr, git, hg, ...)
(use-package pcmpl-args
  :ensure t
  :after pcomplete)

;;; pcmpl-pip --- pcomplete for pip
;; Pcomplete for pip.
;; Based on pip 1.5.6 docs.
(use-package pcmpl-pip
  :ensure t
  :after pcomplete)

;;; pcmpl-git --- pcomplete for git
;;
;; Complete both git commands and their options and arguments. Type
;; '-' to complete short options and '--' to complete long options.
;; For commands that accept commit as argument, branches and tags
;; will be tried first and then the sha1's in the first few hundred
;; commits.
(use-package pcmpl-git
  :ensure t
  :after pcomplete)

;;; pcomplete-extension --- An emacs extension that provide enhanced
;;;                         completion in (e)shell buffers
;; Provide completion on args in folowing commands:
;;
;;   * find
;;   * xargs
;;   * cd
;;   * ls
;;   * hg (mercurial)
;;   * apt-get
;;   * sudo (make most commands working after sudo)
(use-package pcomplete-extension
  :ensure t
  :after pcomplete)

;;; load-bash-alias.el --- Convert bash aliases into eshell ones
;; Convert bash aliases into eshell ones
(use-package load-bash-alias
  :ensure t
  :hook (eshell-mode . load-bash-alias-into-eshell)
  :custom
  (load-bash-alias-bashrc-file "~/.bash_aliases"))

(el-init-provide)



;;; init-eshell.el ends here
