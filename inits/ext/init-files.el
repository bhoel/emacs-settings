;;; init-files.el --- Settings for `init-files.el'.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:40:08 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)
(require 'cl-lib)

(require 'bind-key)

;;; files.el --- file input and output commands for Emacs
(use-package files
  :demand t
  :functions backup-file-name-p
  :bind (([f9] . recover-file)
         :map ctl-x-map
         ("C-k" . kill-some-buffers))
  :custom
  (backup-by-copying t)
  (find-file-existing-other-name t)
  (find-file-visit-truename t)
  (backup-directory-alist `((".*" . ,(concat user-emacs-local-directory "backups"))))
  (require-final-newline t)
  :config
  ;; https://www.emacswiki.org/emacs/BackupDirectory
  ;; If you visit the backup directory from time to time to retrieve
  ;; an old file version then it’s a good idea to prevent the
  ;; directory from cluttering up with very old backup files. Put this
  ;; into your .emacs to automatically purge backup files not accessed
  ;; in a week:
  (message "Deleting old backup files...")
  (when (boundp 'user-emacs-local-directory)
    (let (;(maxage (* 60 60 24 7)) ; one week
          (maxage (* 60 60 24 7 5)) ; five weeks
          (current (float-time (current-time))))
      (dolist (file (directory-files
                     (concat user-emacs-local-directory "backups") t))
	(when (and (backup-file-name-p file)
                   (> (- current (float-time (cl-fifth (file-attributes file))))
                      maxage))
          (message "%s" file)
          (delete-file file)))))
  (message "Deleting old backup files...done"))

;;; archive-rpm.el --- RPM support for archive-mode
;; This module adds support for RPM archives to archive-mode.
;;
;; RPM files consist of metadata plus a compressed CPIO archive, so
;; this module relies on `archive-cpio'.
(use-package archive-rpm :ensure t)

(el-init-provide)



;;; init-files.el ends here
