;;; init-flycheck.el --- Settings for `flycheck'.

;; Copyright © 2013, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:57:53 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(use-package init-my-switches :load-path "inits")
(when my-isux
  (use-package init-linux-irony :load-path "inits/ext"))

;;; flycheck.el --- On-the-fly syntax checking
(use-package flycheck
  :ensure t
  :if (not my-iswin)
  :hook (((cython-mode python-mode) . flycheck-mode)
         (after-init . global-flycheck-mode)))

;;; flycheck-pos-tip.el --- Display Flycheck errors in GUI tooltips
(use-package flycheck-pos-tip
  :ensure t
  :if (not my-iswin)
  :commands flycheck-pos-tip-mode flycheck-pos-tip-error-messages
  :hook (after-init . flycheck-pos-tip-mode)
  :custom
  (flycheck-display-errors-function #'flycheck-pos-tip-error-messages))

;;; flycheck-popup-tip --- Display Flycheck error messages using popup.el
;; It displays Flycheck error messages in buffer using `popup.el' library.
;;
;; For more information about Flycheck:
;; http://www.flycheck.org/
;; https://github.com/flycheck/flycheck
;;
;; For more information about this Flycheck extension:
;; https://github.com/flycheck/flycheck-popup-tip
(use-package flycheck-popup-tip
  :ensure t
  :hook ((flycheck-mode . flycheck-popup-tip-mode)))

;;; flycheck-color-mode-line.el --- Change mode line color with
;;;                                 Flycheck status
(use-package flycheck-color-mode-line
  :ensure t
  :hook ((flycheck-mode . flycheck-color-mode-line-mode)))

;;; helm-flycheck.el --- Show flycheck errors with helm
(use-package helm-flycheck
  :ensure t
  :after (:all helm flycheck)
  :bind (
         :map flycheck-mode-map
         ("C-c ! h" . helm-flycheck)))

;;; flycheck-tip.el --- Show flycheck/flymake errors by tooltip
;; URL: https://github.com/yuutayamada/flycheck-tip
(use-package flycheck-tip
  :ensure t
  :bind (
         :map prog-mode-map
         ("C-c C-n" . flycheck-tip-cycle))
  :config
  ;; If you build Emacs with D-Bus option, you may configure following
  ;; setting. This keeps the errors on notification area. Please check
  ;; ‘error-tip-notify-timeout’ to change limit of the timeout as
  ;; well.
  (when (featurep 'dbusbind)
    (setq error-tip-notify-keep-messages t)))

(el-init-provide)



;;; flycheck-settings.el ends here
