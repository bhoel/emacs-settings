;;; init-goto-chg.el --- Settings for `goto-chg'.

;; Copyright © 2014, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 19:55:34 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

;;; goto-chg.el --- goto last change
(use-package goto-chg
  :ensure t
  :if my-isux
  :bind (("C-."   . goto-last-change)
         ("S-C-." . goto-last-change)
         ;; M-. can conflict with etags tag search. But C-. can get
         ;; overwritten by flyspell-auto-correct-word. And
         ;; goto-last-change needs a really fast key.
         ("M-."   . goto-last-change)))

(el-init-provide)



;;; init-goto-chg.el ends here
