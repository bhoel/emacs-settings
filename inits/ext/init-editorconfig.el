;;; init-editorconfig.el --- Set up `.editorconfig` support for emacs.

;; Copyright © 2020, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:40:08 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;;; editorconfig.el --- EditorConfig Emacs Plugin
;; EditorConfig helps developers define and maintain consistent
;; coding styles between different editors and IDEs.
;;
;; The EditorConfig project consists of a file format for defining
;; coding styles and a collection of text editor plugins that enable
;; editors to read the file format and adhere to defined styles.
;; EditorConfig files are easily readable and they work nicely with
;; version control systems.
(use-package editorconfig
  :ensure t
  :functions editorconfig-mode
  :config
  (editorconfig-mode 1))

;;; editorconfig-charset-extras.el --- Extra EditorConfig Charset Support
;; This library adds extra charset supports to editorconfig-emacs.
;; The list of supported charsets is taken from the result of
;; `coding-system-list'.
(use-package editorconfig-charset-extras
  :ensure t
  :after editorconfig
  :functions editorconfig-charset-extras
  :config
  (add-hook 'editorconfig-after-apply-functions #'editorconfig-charset-extras))

;;; editorconfig-custom-majormode.el --- Decide major-mode and mmm-mode from EditorConfig
;; An EditorConfig extension that defines a property to specify which
;; Emacs major-mode to use for files.
(use-package editorconfig-custom-majormode
  :ensure t
  :after editorconfig
  :functions editorconfig-custom-majormode
  :config
  (add-hook 'editorconfig-after-apply-functions #'editorconfig-custom-majormode))

;;; editorconfig-domain-specific.el --- Apply brace style and other "domain-specific" EditorConfig properties
;; Thanks to @10sr. editorconfig-custom-majormode.el taught me how to
;; make an editorconfig hook.
(use-package editorconfig-domain-specific
  :ensure t
  :after editiorconfig
  :functions editorconfig-domain-specific
  :config
  (add-hook 'editorconfig-after-apply-functions #'editorconfig-domain-specific))

;;; editorconfig-generate.el --- Generate .editorconfig
;; Generate .editorconfig content for buffer from current Emacs
;; configuration.
;;
;; `M-x editorconfig-generate` to open a buffer that has
;; `.editorconfig` content for current buffer. Properties are
;; extracted from current Emacs configurations.
(use-package editorconfig-generate :ensure t)

(el-init-provide)



;;; init-editorconfig.el ends here
