;;; init-helm.el --- Starting helm

;; Copyright (C) 2013, 2022 Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-06 17:55:51 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package)
  (require 'ibuffer))

(require 'el-init)

(require 'bind-key)

(require 'init-my-switches)
(require 'init-eshell)
(require 'init-org)

;;; helm-icons.el --- Helm icons
(use-package helm-icons
  :ensure t
  :hook ((helm-before-initialize . helm-icons-enable)))

(use-package helm-global-bindings
  :ensure helm
  :custom
  ;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
  ;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
  ;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.
  (helm-command-prefix-key "C-c h"))

;;; helm-mode.el --- Enable helm completion everywhere.
(use-package helm-mode
  :ensure helm
  :hook ((after-init . helm-mode))
  :defines helm-completing-read-handlers-alist
  :custom
  (completion-styles '(flex)))
;; (completion-styles (nconc completion-styles '(flex)))

;;; helm.el --- Emacs incremental and narrowing framework
(use-package helm
  :ensure t
  :bind (
         :map esc-map
         ("x" . helm-M-x)
         :map helm-map
         ;; rebind tab to do persistent action
         ("<tab>" . helm-execute-persistent-action)
         ;; make TAB works in terminal
         ("C-i" . helm-execute-persistent-action)
         ;; list actions using C-z
         ("C-z" . helm-select-action))
  :hook ((after-init . helm-autoresize-mode)
         (helm-minibuffer-set-up . helm-hide-minibuffer-maybe))
  :custom
  ;; open helm buffer inside current window, not occupy whole other
  ;; window
  (helm-split-window-inside-p nil)
  ;; move to end or beginning of source when reaching top or bottom
  ;; of source.
  (helm-move-to-line-cycle-in-source t)
  ;; Send current input in header-line when non-nil.
  (helm-echo-input-in-header-line t)
  ;; Display header-line when non nil.
  (helm-display-header-line nil)
  ;; Idle time before updating, specified in seconds.
  (helm-input-idle-delay 0.01)
  (helm-reuse-last-window-split-state t)
  (helm-always-two-windows t)
  (helm-commands-using-frame '(completion-at-point
			       helm-apropos
			       helm-eshell-prompts
			       ;; helm-semantic-or-imenu
			       helm-imenu-in-all-buffers))
  (helm-actions-inherit-frame-settings t)
  (helm-use-frame-when-no-suitable-window t)
  (helm-show-action-window-other-window 'left)
  (helm-allow-mouse t)
  (helm-autoresize-max-height 80) ; it is %.
  (helm-autoresize-min-height 20) ; it is %.
  (helm-follow-mode-persistent t)
  (helm-candidate-number-limit 500)
  :config
  (defun helm-hide-minibuffer-maybe ()
    (when (with-helm-buffer helm-echo-input-in-header-line)
      (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
        (overlay-put ov 'window (selected-window))
        (overlay-put ov 'face (let ((bg-color (face-background 'default nil)))
                                `(:background ,bg-color :foreground ,bg-color)))
        (setq-local cursor-type nil)))))

;;; helm-lib.el --- Helm routines.
;; All helm functions that don't require specific helm code should go here.
(use-package helm-lib
  :ensure helm
  :functions helm-buffer-get
  :custom
  ;; scroll 4 lines other window using M-<next>/M-<prior>
  (helm-scroll-amount 4))

;;; helm-grep.el --- Helm Incremental Grep.
(use-package helm-grep
  :ensure helm
  :bind (
         :map helm-grep-mode-map
         ("<return>" . helm-grep-mode-jump-other-window)
         ("n" . helm-grep-mode-jump-other-window-forward)
         ("p" . helm-grep-mode-jump-other-window-backward))
  :custom
  ;; Default command to read pdf files from pdfgrep. Where ’%f’
  ;; format spec is filename and ’%p’ is page number.
  (helm-pdfgrep-default-read-command "evince --page-label=%p '%f'")
  (helm-grep-default-command "ack -Hn --color --smart-case --no-group %e %p %f")
  (helm-grep-default-recurse-command "ack -H --color --smart-case --no-group %e %p %f")
  (helm-grep-ag-command "rg --color=always --smart-case --no-heading --line-number %s %s %s")
  (helm-grep-git-grep-command "git --no-pager grep -n%cH --color=always --exclude-standard --no-index --full-name -e %p -- %f"))

;;; helm-adaptive.el --- Adaptive Sorting of Candidates.
(use-package helm-adaptive
  :hook (after-init . helm-adaptive-mode))

;;; helm-sys.el --- System related functions for helm.
(use-package helm-sys
  :functions helm-top helm-top-poll-mode
  :config
  (helm-top-poll-mode 1))

;;; helm-info.el --- Browse info index with helm
(use-package helm-info
  :ensure helm
  :bind (("C-h r" . helm-info-emacs)))

;;; helm-ring.el --- kill-ring, mark-ring, and register browsers for helm.
(use-package helm-ring
  :ensure helm)

;;; helm-files.el --- helm file browser and related.
(use-package helm-files
  :ensure helm
  :bind (
         :map ctl-x-map
         ("C-f" . helm-find-files))
  :custom
  ;; Use `recentf-list' instead of `file-name-history' in
  ;; `helm-find-files'.
  (helm-ff-file-name-history-use-recentf t)
  ;; search for library in `require' and `declare-function' sexp.
  (helm-ff-search-library-in-sexp t)
  (helm-dwim-target 'next-window))

;;; helm-for-files.el --- helm-for-files and related.
(use-package helm-for-files
  :ensure helm
  :bind ("C-c r" . helm-recentf)
  :custom
  (helm-turn-on-recentf t))

;;; helm-buffers.el --- helm support for buffers.
(use-package helm-buffers
  :ensure helm
  :defines helm-buffers-favorite-modes
  :bind (
         :map ctl-x-map
         ("b" . helm-buffers-list))
  :custom
  ;; Ignore checking for ‘file-exists-p’ on remote files.
  (helm-buffer-skip-remote-checking t)
  ;; fuzzy matching buffer names when non--nil
  (helm-buffers-fuzzy-matching t)
  (helm-buffer-max-length 22)
  (helm-buffers-end-truncated-string "…")
  (helm-buffers-maybe-switch-to-tab t))

;;; helm-imenu.el --- Helm interface for Imenu
(use-package helm-imenu
  :ensure helm
  :custom
  ;; Enable fuzzy matching in ‘helm-source-imenu’.
  (helm-imenu-fuzzy-match t)
  (helm-imenu-lynx-style-map t))

;;; helm-elisp.el --- Elisp symbols completion for helm.
(use-package helm-elisp
  :ensure helm
  :bind (("C-h C-f" . helm-apropos)
         ("C-h C-l" . helm-locate-library))
  :custom
  ;; Enable fuzzy matching in emacs-lisp completion.
  (helm-lisp-fuzzy-completion t)
  ;; Enable fuzzy matching for ‘helm-apropos’.
  (helm-apropos-fuzzy-match t))

;;; helm-locate.el --- helm interface for locate.
(use-package helm-locate
  :ensure helm
  :custom
  ;; Enable fuzzy matching in ‘helm-locate’.
  (helm-locate-fuzzy-match t))

;;; helm-man.el --- Man and woman UI
(use-package helm-man
  :ensure helm)

;;; helm-occur.el --- Incremental Occur for Helm.
(use-package helm-occur
  :ensure helm
  :bind (("M-s o" . helm-occur)))

;;; helm-eshell.el --- pcomplete and eshell completion for helm.
(use-package helm-eshell
  :after eshell-mode
  :bind (
         :map eshell-mode-map
         ("M-r" . helm-eshell-history)))

;;; helm-utils.el --- Utilities Functions for helm.
(use-package helm-utils
  :hook (;; Save current position to mark ring
         (helm-goto-line-before . helm-save-current-pos-to-mark-ring)
         ;; find-file-hook
         (find-file . helm-save-current-pos-to-mark-ring))
  ;; Popup buffer-name or filename in grep/moccur/imenu-all etc...
  :functions helm-window-mosaic-fn helm-popup-tip-mode
  :custom
  (helm-highlight-matches-around-point-max-lines 30)
  (helm-window-show-buffers-function #'helm-window-mosaic-fn)
  :config
  (helm-popup-tip-mode 1))

;;; helm-misc.el --- Various functions for helm -*- lexical-binding: t -*-
(use-package helm-misc
  :ensure helm
  ;; show minibuffer history with Helm
  :bind (
         :map minibuffer-local-map
         ("M-p" . helm-minibuffer-history)
         ("M-n" . helm-minibuffer-history)))

;;; helm-external --- Run Externals commands within Emacs with helm completion.
(use-package helm-external
  :ensure helm
  :custom
  ;; A shell command to jump to a window running specific program.
  ;; Need external program wmctrl.
  (helm-raise-command "wmctrl -xa %s")
  ;; Default external file browser for your system. Directories will
  ;; be opened externally with it when opening file externally in
  ;; ‘helm-find-files’. Set to nil if you do not have external file
  ;; browser or do not want to use it. Windows users should set that
  ;; to "explorer.exe".
  (helm-default-external-file-browser "thunar"))

;;; helm-files.el --- helm file browser and related.
(use-package helm-files
  :custom
  ;; Auto update when only one candidate directory is matched.
  ;; Default value when starting ‘helm-find-files’ is nil to not confuse
  ;; new users.
  ;; For a better experience with ‘helm-find-files’ set this to non--nil
  ;; and use C-<backspace> to toggle it.
  (helm-ff-auto-update-initial-value t)
  ;; helm-tramp-verbose 6
  (helm-ff-allow-non-existing-file-at-point t)
  (helm-ff-lynx-style-map t))

;;; helm-ls-git --- Yet another helm for listing the files in a git repo.
(use-package helm-ls-git
  :ensure t
  :after magit
  :custom
  (helm-ls-git-status-command 'magit-status-internal))

;;; helm-dictionary.el --- Helm source for looking up dictionaries
;; This helm source can be used to look up words in local (offline)
;; dictionaries.  It also provides short-cuts for various online
;; dictionaries, which is useful in situations where the local
;; dictionary doesn't have an entry for a word.
;;
;; Dictionaries are available for a variety of language pairs.  See
;; the project page for an incomplete list:
;;
;;     https://github.com/emacs-helm/helm-dictionary
(use-package helm-dictionary
  :ensure
  :custom
  (helm-dictionary-database "/usr/share/dict/de-en.txt"))

;;; helm-hunks --- A helm interface for git hunks
;; A helm interface for browsing unstaged git hunks.
;;
;; Enable `helm-follow-mode' and trigger `helm-hunks' to jump around
;; unstaged hunks like never before.
;;
;; Run `helm-hunks-current-buffer' to jump around the current buffer only.
;;
;; Run `helm-hunks-staged' to jump around staged hunks, unstage with `C-u'.
;;
;; Run `helm-hunks-staged-current-buffer' to jump around staged hunks in
;; the current buffer only.
;;
;; Kill hunks you wish undone with `C-k'.
;;
;; Preview hunk changes with `C-c C-p', and jump to hunks in "other window"
;; or "other frame" with `C-c o' and `C-c C-o', respectively.
;;
;; Commit with `C-c C-c`, amend with `C-c C-a`.
;;
;; Quit with `C-c C-k'.
;;
;; Credits/inspiration: git-gutter+ - https://github.com/nonsequitur/git-gutter-plus/
(use-package helm-hunks
  :ensure t)

;;; helm-descbinds.el --- Yet Another `describe-bindings' with `helm'.
(use-package helm-descbinds
  :ensure t
  :functions helm-descbinds-mode
  :config
  (helm-descbinds-mode))

(use-package emacs
  :custom
  ;; Avoid hitting forbidden directory .gvfs when using find.
  (completion-ignored-extensions
   (append completion-ignored-extensions '(".gvfs/" ".dbus/" "dconf/"))))

(el-init-provide)



;;; init-helm/home/hoel/.emacs.d/inits/ext/init-projectile.el ends here
