;;; init-bm.el --- Settings for `bm.el'

;; Copyright © 2014, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:53:01 hoel>

;;; Commentary:

;; This package was created because I missed the bookmarks from M$
;; Visual Studio. I find that they provide an easy way to navigate in
;; a buffer.
;;
;; bm.el provides visible, buffer local, bookmarks and the ability to
;; jump forward and backward to the next bookmark.

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

(require 'bind-key)

(use-package init-my-switches :load-path "inits")

;;; bm --- Visible bookmarks in buffer.
(use-package bm
  :ensure t
  :if (not my-iswin)
  :bind (
         ;; To make it easier to use, assign the commands to some keys.
         ([C-f6] . bm-toggle)
         ([f6] . bm-next)
         ([S-f6] . bm-previous)
         ;; Click on fringe to toggle bookmarks, and use mouse wheel
         ;; to move between them.
         ([left-fringe mouse-5] . bm-next-mouse)
         ([left-fringe mouse-4] . bm-previous-mouse)
         ([left-fringe mouse-1] . bm-toggle-mouse))
  :custom
  ;; If you would like to cycle bookmark in LIFO order, add the
  ;; following line:
  (bm-in-lifo-order t)
  ;; If you would like to cycle through bookmarks in all open buffers,
  ;; add the following line:
  (bm-cycle-all-buffers t)
  :init
  ;; If you would like the markers on the right fringe instead of the
  ;; left, add the following line:
  (setq bm-marker 'bm-marker-right))

(el-init-provide)



;;; init-bm.el ends here
