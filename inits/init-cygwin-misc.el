;;; init-cygwin-misc.el --- Misc settings for cygwin emacs.

;; Copyright © 2016, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:12 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'quelpa)
  (require 'quelpa-use-package)
  (require 'use-package))

(require 'el-init)

;;; cygwin-mount.el --- Teach EMACS about cygwin styles and mount
;;;                     points.
(use-package cygwin-mount
  :after setup-cygwin
  :quelpa ((cygwin-mount :fetcher wiki)
	   :upgrade t)
  :commands cygwin-mount-activate
  :init
  (setq cygwin-mount-cygwin-bin-directory
        (cond
         ((file-exists-p "c:/cygwin/bin/mount.exe") "c:/cygwin/bin/"))))

;;; setup-cygwin.el --- Set up Emacs for using Cygwin
(use-package setup-cygwin
  :quelpa ((setup-cygwin :fetcher wiki)
	   :upgrade t)
  :init
  (setenv "PATH" (concat (getenv "PATH") ";C:/cygwin64/bin")))

(el-init-provide)



;;; init-cygwin-misc.el ends here
