;;; init-epa.el --- Initialize `epa-file'.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-04 20:39:54 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;;; epa-file.el --- the EasyPG Assistant, transparent file encryption
;; Setup for transparent, automatic encryption and decryption:
(use-package epa-file
  :defines epa-file-cache-passphrase-for-symmetric-encryption
  :functions epa-file-enable
  :custom
  (epa-file-cache-passphrase-for-symmetric-encryption t "saving password"))
;; (setenv "GPG_AGENT_INFO" nil)                              ; non-GUI password dialog.
(unless (memq epa-file-handler file-name-handler-alist)
  (epa-file-enable)
  (setq epg-pinentry-mode 'loopback))

;;; pinentry.el --- GnuPG Pinentry server implementation
(use-package pinentry
  :ensure
  :functions pinentry-start
  :config
  (pinentry-start))

(el-init-provide)



;;; init-epa.el ends here
