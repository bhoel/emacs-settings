;;; init-minibuffer.el --- Settings for `minibuffer'.

;; Copyright © 2015, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:11 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)


;;; minibuffer.el --- Minibuffer completion functions
(use-package minibuffer
  :custom
  ;; I would like to be able to find multiple files in one command,
  ;; using a wildcard, e.x. *.cpp. I am quite sure that someone have
  ;; already written this. Well, have he?
  ;;
  ;; Answer:
  ;; (setq completion-styles '(basic partial-completion emacs22))
  (completion-styles (append completion-styles '(initials)))
  ;; Treat the SPC or - inserted by ‘minibuffer-complete-word’ as delimiters.
  ;; Those chars are treated as delimiters if this variable is non-nil.
  ;; I.e. if non-nil, M-x SPC will just insert a "-" in the minibuffer, whereas
  ;; if nil, it will list all possible commands in *Completions* because none of
  ;; the commands start with a "-" or a SPC.
  (completion-pcm-complete-word-inserts-delimiters t)
  (completions-detailed t))

(el-init-provide)



;;; init-minibuffer.el ends here
