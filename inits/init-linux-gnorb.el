;;; init-linux-gnorb.el --- gnorb startup file. -*- lexical-binding: t; -*-

;; Copyright © 2017, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-08-03 22:57:11 hoel>

;;; Commentary:

;;; Code:

(eval-when-compile
  (require 'package)
  (require 'use-package))

(require 'el-init)

;; ;; (use-package gnorb
;; ;;   :ensure t
;; ;;   :config
;;   (with-eval-after-load 'gnus
;;     (add-to-list 'gnus-secondary-select-methods '(nngnorb "gnorb") t)
;;     (gnus-registry-initialize)
;;     ;;(gnorb-install-defaults)
;;     (gnorb-tracking-initialize))
;; ;;)

;;   (use-package gnorb
;;     :ensure t
;;     :after (:all gnus org)
;;     :config
;; (require 'org-id)
;; (require 'gnus-registry)
;;     (add-to-list 'gnus-secondary-select-methods '(nngnorb "gnorb") t))
;;     (gnus-registry-initialize)
;;     ;;(gnorb-install-defaults)
;;     (gnorb-tracking-initialize)

(el-init-provide)



;;; init-linux-gnorb.el ends here
