;;; init.el --- user init file            -*- no-byte-compile: t; lexical-binding: t -*-

;; Copyright © 2004, 2013, 2014, 2015, 2020, 2021, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;; Time-stamp: <2023-12-30 20:27:08 hoel>

;;; Commentary:

;;; Code:

(setq
 ;; Number of bytes of consing between garbage collections.
 ;; Garbage collection can happen automatically once this many bytes have been
 ;; allocated since the last garbage collection.  All data types count.
 gc-cons-threshold (* 100 1024 1024)
 ;; Maximum number of bytes to read from subprocess in a single chunk.
 ;; Enlarge the value only if the subprocess generates very large (megabytes)
 ;; amounts of data in one go.
 read-process-output-max(* 1024 1024))

;; Debuging only
;; -------------
;; (open-dribble-file (expand-file-name "~/.dribble"))
;; (open-termscript (expand-file-name "~/.termscript"))

;; (setq byte-compile-error-on-warn t)

;; from <https://andrewjamesjohnson.com/suppressing-ad-handle-definition-warnings-in-emacs/>
;; Suppressing ad-handle-definition Warnings in Emacs
;; Andrew Johnson
;; As I have been tweaking my Emacs configuration, I noticed that I
;; was getting warnings like the following in *Messages* during Emacs
;; startup:
;; ad-handle-definition: `tramp-read-passwd' got redefined
;;
;; I looked into it and these warnings are generated when functions
;; are redefined with defadvice. Most of the warnings I received were
;; coming from third-party packages I had installed, not my own
;; configuration. In any case, they were not helpful, just noise
;; during Emacs startup.
;;
;; Like everything in Emacs, this is configurable. Turning off the
;; warnings is as easy as adding:
(setq ad-redefinition-action 'accept)
;; To your .emacs file. I found I had to add it fairly early to ensure
;; that all the warnings were removed.

;; Non-nil means `load' prefers the newest version of a file.
(setq load-prefer-newer t)

;;; package.el --- Simple package system for Emacs
;;; Convenient package handling in emacs
;; Add more ELPA package sources.
(require 'package)
(setq package-native-compile t)
(nconc package-archives
       '(("melpa-stable" . "https://stable.melpa.org/packages/")
	 ("melpa" . "https://melpa.org/packages/")))

;; Check if package information exists for all sources. (usually only
;; required for first start).
(unless
    (cl-every #'file-exists-p
	      (mapcar (lambda (package)
			(concat
			 package-user-dir "archives/" (car package)
			 "/archive-contents"))
		      package-archives))
  (package-refresh-contents))

;; install remaining packages
(package-install-selected-packages)
;; remove packages not required anymore
(package-autoremove)

;;; auto-compile.el --- automatically compile Emacs Lisp libraries
(unless (package-installed-p 'auto-compile)
  (package-install 'auto-compile))
(require 'auto-compile)
(auto-compile-on-load-mode)
;; (auto-compile-on-save-mode)

;;; quelpa.el --- Emacs Lisp packages built directly from source
(unless (package-installed-p 'quelpa-use-package)
  (package-install 'quelpa-use-package))
(customize-set-value 'quelpa-dir
		     (concat user-emacs-local-directory
			     "quelpa-"
			     (number-to-string emacs-major-version)))
(customize-set-value 'quelpa-checkout-melpa-p nil)
(customize-set-value 'quelpa-build-explicit-tar-format-p t)
(require 'quelpa)
(require 'quelpa-use-package)

;;; use-package.el --- A configuration macro for simplifying your .emacs
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-compute-statistics t)

;;; initchart --- Emacs' init process performance visualization
;; initchart.el provides macros and functions to measure and visualize
;; a init process of Emacs.
;;
;; First, one need to record the execution time of some primary
;; functions such as 'load' and 'require'. Use the macro
;; 'initchart-record-execution-time-of' at the beginning of your
;; init.el to register functions of concern, and then launch an Emacs
;; process as usual. Execution time of the registered functions will
;; be logged in the '*initchart*' buffer.
;;
;; Then, you can visualize these logs by invoking the command
;; 'initchart-visualize-init-sequence', which will ask you the
;; filepath to save an output SVG image.
(use-package initchart
  :disabled
  :quelpa ((initchart
	    :repo "yuttie/initchart"
	    :fetcher github)
	   :upgrade t)
  :commands initchart-record-execution-time-of
  :hook (after-init . initchart-visualize-init-sequence)
  :config
  (initchart-record-execution-time-of load file)
  (initchart-record-execution-time-of require feature))

;;; benchmark-init --- Benchmarks Emacs require and load calls
;; This is a simple benchmark of calls to Emacs require and load
;; functions. It can be used to keep track of where time is being
;; spent during Emacs startup in order to optimize startup times.
;;
;; The code was originally based on init-benchmarking.el by Steve
;; Purcell but several modification has gone into it since.
(use-package benchmark-init
  :ensure t
  :disabled
  :demand t
  :hook (after-init . benchmark-init/deactivate)
  :config
  (benchmark-init/activate))

;;; el-init --- A loader inspired by init-loader
(use-package el-init
  :ensure t
  :functions el-init-load
  :preface
  ;; Initialize installed packages
  ;; Some init files are relevant for cygwin environment only.
  (defun el-init-cygwin-p ()
    "Return non-nil if Emacs is running on cygwin.
This is used by `el-init-require/system-case'."
    (string-match-p "cygwin" system-configuration))
  ;; Skip some init files regardless.
  (defun el-init-skip-p ()
    "Return non to skip loading this init file.
This is used by `el-init-require/system-case'."
    nil)
  :config
  (nconc el-init-system-case-alist
	 '(("^init-cygwin-" . el-init-cygwin-p)
	   ("^init-skip-" . el-init-skip-p)))
  (el-init-load
   (concat user-emacs-directory "inits")
   :subdirectories '("." "ext" "progmodes")
   :wrappers '(el-init-require/record-error
	       el-init-require/system-case
	       el-init-require/compile-old-library
	       el-init-require/record-eval-after-load-error
	       el-init-require/record-old-library
	       el-init-require/benchmark)))

;;; el-init-viewer --- Record viewer for el-init
(use-package el-init-viewer
  :ensure t
  :commands el-init-viewer)

(require 'init-path)
(require 'init-my-switches)

;;; desktop.el --- save partial status of Emacs when killed
(use-package desktop
  :preface
  (defun my@after@init@wait@hook@fun ()
    (when my-ishome
      (message "press any key to load desktop.")
      (read-event))
    ;; restore desktop
    (desktop-read))
  :hook ((after-init . my@after@init@wait@hook@fun))
  :custom
  (desktop-base-file-name
   (concat ".emacs.desktop." (system-name))
   "Name of file for Emacs desktop, excluding the directory part.")
  (desktop-base-lock-name
   (concat ".emacs.desktop." (system-name) ".lock")
   "Name of lock file for Emacs desktop, excluding the directory part.")
  (desktop-buffers-not-to-save
   (rx (and bos (or " "
		    "*"
		    (: ".type-break" eos)
		    (: ".notes" eos)
		    (: "diary" eos))))
   "Identifying buffers that are to be excluded from saving.")
  (desktop-files-not-to-save
   (rx (group (or (: bol (: "/" (zero-or-more (not (any "/" ":"))) ":" ) eol)
		  (: "(ftp)" eol)
		  (: bol ".type-break" eol))))
   "Identifying files whose buffers are to be excluded from saving.")
  (desktop-restore-eager 2 "Number of buffers to restore immediately.")
  (desktop-save-mode 1 "save desktop")
  :config
  ;; "List of major modes whose buffers should not be saved."
  (setq desktop-modes-not-to-save
	(nconc desktop-modes-not-to-save
	       '(dired-mode
		 special-mode
		 Info-mode
		 fundamental-mode
		 compilation-mode
		 info-lookup-mode)))
  (setq desktop-minor-mode-table
	(nconc desktop-minor-mode-table
	       '((anzu-mode nil)
		 (auto-compile-on-load-mode nil)
		 (diff-hl-mode nil)
		 (helm-mode nil)
		 (page-break-lines-mode nil)
		 (persistent-scratch-autosave-mode nil)
		 (smartparens-global-mode nil)
		 (type-break-mode nil)
		 (undo-tree-mode nil)
		 (unicode-troll-stopper-mode nil)
		 (yas-minor-mode nil))))
  (setq desktop-dirname user-emacs-local-directory))

;;; frameset.el --- save and restore frame and window setup
(use-package frameset
  :config
  (dolist (filter
	   '((foreground-color . :never)
	     (background-color . :never)
	     (font . :never)
	     (cursor-color . :never)
	     (background-mode . :never)
	     (ns-appearance . :never)
	     (background-mode . :never)
	     (frameset--text-pixel-height . :never)
	     (frameset--text-pixel-width . :never)
	     (GUI:font . :never)))
    (add-to-list 'frameset-filter-alist filter)))


;; TODO ############### Move the following settings !!!!!!!!!!!!!!

;;; punctuality-logger --- Punctuality logger for Emacs
;; This package helps you keep track of when you are on time
;; and when you are late to your various appointments.
(use-package punctuality-logger :ensure t)

;;; type-break.el --- encourage rests from typing at appropriate intervals
(use-package type-break
  :if my-isux
  :functions type-break-mode
  :custom
  (type-break-file-name
   (concat user-emacs-local-directory ".type-break")
   "Name of file used to save state across sessions.")
  ;; If you prefer not to be queried about taking breaks, but instead just
  ;; want to be reminded, do the following:
  (type-break-demo-functions '(type-break-demo-boring))
  :config
  (type-break-mode))

(use-package emacs
  :custom
  ;; Number of input events between auto-saves.
  ;; Zero means disable autosaving due to number of characters typed.
  (auto-save-interval 100)

  ;; Non-nil means highlight region even in nonselected windows.
  (highlight-nonselected-windows t)

  ;; How many columns to scroll the window when point gets too close to the edge.
  ;; When point is less than ‘hscroll-margin’ columns from the window
  ;; edge, automatic hscrolling will scroll the window by the amount of columns
  ;; determined by this variable.  If its value is a positive integer, scroll that
  ;; many columns.  If it’s a positive floating-point number, it specifies the
  ;; fraction of the window’s width to scroll.  If it’s nil or zero, point will be
  ;; centered horizontally after the scroll.  Any other value, including negative
  ;; numbers, are treated as if the value were zero.
  ;;
  ;; Automatic hscrolling always moves point outside the scroll margin, so if
  ;; point was more than scroll step columns inside the margin, the window will
  ;; scroll more than the value given by the scroll step.
  ;;
  ;; Note that the lower bound for automatic hscrolling specified by ‘scroll-left’
  ;; and ‘scroll-right’ overrides this variable’s effect.
  (hscroll-step 5)

  ;; Non-nil inhibits the startup screen.
  ;;
  ;; This is for use in your personal init file (but NOT site-start.el),
  ;; once you are familiar with the contents of the startup screen.
  (inhibit-startup-screen t)

  ;; Default number of context lines included around
  ;; ‘list-matching-lines’ matches. A negative number means to include
  ;; that many lines before the match. A positive number means to
  ;; include that many lines both before and after.
  (list-matching-lines-default-context-lines 3)

  ;; Limit on number of Lisp variable bindings and ‘unwind-protect’s.
  ;; If Lisp code tries to increase the total number past this amount,
  ;; an error is signaled.
  ;; You can safely use a value considerably larger than the default value,
  ;; if that proves inconveniently small.  However, if you increase it too far,
  ;; Emacs could run out of memory trying to make the stack bigger.
  ;; Note that this limit may be silently increased by the debugger
  ;; if ‘debug-on-error’ or ‘debug-on-quit’ is set.
  (max-specpdl-size 20000)

  ;; The number of lines to try scrolling a window by when point moves out.
  ;; If that fails to bring point back on frame, point is centered instead.
  ;; If this is zero, point is always centered after it moves off frame.
  ;; If you want scrolling to always be a line at a time, you should set
  ;; ‘scroll-conservatively’ to a large value rather than set this to 1.
  (scroll-step 2)

  ;; Non-nil means a single space does not end a sentence.
  ;; This is relevant for filling.  See also ‘sentence-end-without-period’
  ;; and ‘colon-double-space’.
  ;;
  ;; This value is used by the function ‘sentence-end’ to construct the
  ;; regexp describing the end of a sentence, when the value of the variable
  ;; ‘sentence-end’ is nil.  See Info node ‘(elisp)Standard Regexps’.
  ;;
  ;; This variable is safe as a file local variable if its value
  ;; satisfies the predicate ‘booleanp’.
  (sentence-end-double-space nil)

  ;; Non-nil means try to flash the frame to represent a bell.
  ;;
  ;; See also ‘ring-bell-function’.
  (visible-bell t))



;;; init.el ends here
