;;; emacs-custom.el --- General customize settings.

;; Copyright © 2014, 2022 by Berthold Höllmann

;; Author: Berthold Höllmann <berhoel@gmail.com>

;;; Commentary:

;;; Code:

(provide 'settings)



(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("7b3ce93a17ce4fc6389bba8ecb9fee9a1e4e01027a5f3532cc47d160fe303d5a" "7e22a8dcf2adcd8b330eab2ed6023fa20ba3b17704d4b186fa9c53f1fab3d4d2" "0e435534351b0cb0ffa265d4cfea16b4b8fe972f41ec6c51423cdf653720b165" "3d4cf45ee28dc5595d8f0a37fc0da519365fd88a2bb98f5c272a50aba86d319b" "5b642a72376abcd7af7889aa945a072f2d2db6694298edc88c728d343fe33733" "fc79a9b611a43d5ebd51b7af654f987f8c481320e6562ca51bc727d6da5d58cf" "df57169b936863059ff48c5d120b84cea26ff809750254b578169e1e246bedc0" "bd19fec77fa640b63cbf22f8328222b3dc4c8f891f63be6ea861187bc6c6c4e4" "8790269696322ff6821d75414c7d6ea8726d204cdeadedfd04c87b0c915296f7" "a0feb1322de9e26a4d209d1cfa236deaf64662bb604fa513cca6a057ddf0ef64" "ab04c00a7e48ad784b52f34aa6bfa1e80d0c3fcacc50e1189af3651013eb0d58" "9a155066ec746201156bb39f7518c1828a73d67742e11271e4f24b7b178c4710" "aae95fc700f9f7ff70efbc294fc7367376aa9456356ae36ec234751040ed9168" "7153b82e50b6f7452b4519097f880d968a6eaf6f6ef38cc45a144958e553fbc6" "04dd0236a367865e591927a3810f178e8d33c372ad5bfef48b5ce90d4b476481" "c5eecfa188e96eef8ee43f689613d625c6c4692a57beeb1c2dd12c07aee889b4" "bb749a38c5cb7d13b60fa7fc40db7eced3d00aa93654d150b9627cabd2d9b361" "1b1e54d9e0b607010937d697556cd5ea66ec9c01e555bb7acea776471da59055" "361b56a6ad844b1fc6690f5d167b92926aca424ee9ae8e4e6952d413cf822903" "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" "df3a4fbc0c536fbef14cc1ff22f88d0eddd259fbde470c0a4831c892860d4661" "fbb449ba8147e2914a9bebc2d6a210b8927313a6c1b70764a7f7c61a9bc04b90" "fa94f0c2ddd30df2bca56ddee6854c5926a8a67125d0c28326fd504e377563a9" "68f7056d6e71be1c1722e4e5c7ff3776edb4fed4f5458927b2e8822715071594" "26614652a4b3515b4bbbb9828d71e206cc249b67c9142c06239ed3418eff95e2" "7c9a41cc8115aaaca56442ea22ab5df89ca1889b93c08d0d42be94e506abbb7e" "a7851f858e2263f3589f272a637c86395f9e308ea5ab416a718019e4137af8f0" "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "a27c00821ccfd5a78b01e4f35dc056706dd9ede09a8b90c6955ae6a390eb1c1e" default))
 '(delete-selection-mode nil)
 '(doom-modeline-check-simple-format nil nil nil "Customized with use-package doom-modeline")
 '(ecb-options-version "2.40")
 '(magit-todos-insert-after '(bottom) nil nil "Changed by setter of obsolete option `magit-todos-insert-at'")
 '(package-install-upgrade-built-in t)
 '(package-selected-packages
   '(chezmoi casual doctest sly-overlay forth-mode glue helm-sly sly sly-asdf sly-repl-ansi-color use-package-ensure-system-package all-the-icons-nerd-fonts project seq bind-key eglot eldoc faceup flymake jsonrpc soap-client tramp verilog-mode pdf-view-pagemark emacsql-sqlite-module sqlite3 jinx magit-stats poke poke-mode nastran-mode beancount py-snippets arduino-cli-mode apdl-mode js-comint js2-mode modus-themes json-mode pandoc-mode my-repo-pins svgo org org-contrib async-await abridge-diff auto-sudoedit blamer quelpa-use-package gitignore-templates git-modes latex-extra po-mode auctex-latexmk unicode-fonts tokei ligature lin mu4e-views csharp-mode csproj-mode vcard electric-cursor gitignore-snippets license-snippets undo-tree direx all-the-icons-ibuffer mmm-mode clang-capf treemacs-all-the-icons lsp-pyright saveplace-pdf-view helm-icons flycheck-plantuml plantuml-mode adoc-mode lsp-java prettier eldoc-eval eldoc-cmake lsp-pyre lsp-latex ccls dap-mode lsp-treemacs helm-lsp lsp-ui fic-mode editorconfig editorconfig-charset-extras editorconfig-custom-majormode editorconfig-domain-specific editorconfig-generate direnv message-attachment-reminder uuidgen go-snippets languagetool gnu-elpa pdb-capf ascii-table modern-fringes company-quickhelp-terminal groovy-mode doom-modeline minimap elpy pinentry emamux dockerfile-mode docker langtool poetry helm-cscope xcscope yasnippet-classic-snippets sysctl format-all qt-pro-mode gnu-elpa-keyring-update isearch-project poly-rst pyvenv dired-git-info treemacs-magit treemacs-icons-dired treemacs-projectile treemacs pack forge load-bash-alias auctex bbdb-vcard eldoc-overlay ob-sql-mode sql-indent quelpa font-lock+ django-commands dklrt flycheck-ledger ledger-mode highlight-doxygen home-end magit-todos filladapt dired-recent org-variable-pitch traad jinja2-mode horoscope org-chef elisp-def cakecrumbs prog-fill projectile graphviz-dot-mode elpygen css-eldoc c-eldoc latexdiff helm-xref pip-requirements emojify-logos helm-ctest dynamic-spaces archive-rpm pulseaudio-control diredfl realgud ini-mode org-projectile-helm circadian yasnippet-snippets r-autoyas flycheck-popup-tip gnus-select-account docker-compose-mode proportional makefile-executor all-the-icons-gnus with-editor vdiff-magit vdiff html2org helm-rtags helm-sql-connect pippel puppet-mode ox-epub lua-mode org-table-sticky-header paperless cov helm-tramp zpresent guess-language test-c smartrep passmm helm-etags-plus swiper xmlgen benchmark-init helm-bbdb all-the-icons-dired all-the-icons web-mode-edit-element auto-compile ein yasnippet helm gxref git-blamed dired-icon magit sphinx-mode robots-txt-mode logview url-http-ntlm rtags cmake-ide cmake-project auto-org-md column-enforce-mode rainbow-delimiters company-irony-c-headers devdocs dictcc dired-launch elf-mode helm-hunks image-dired+ org-attach-screenshot org-bookmark-heading org-preview-html redtick synonymous git-command web-mode live-py-mode flycheck-cython magic-filetype strace-mode sed-mode scad-preview scad-mode company-arduino arduino-mode iss-mode html-check-frag django-manage cpputils-cmake cmake-font-lock cmake-mode modern-cpp-font-lock cff evil notes-mode gmail2bbdb select-themes niceify-info emojify pcomplete-extension pcmpl-git pcmpl-pip pcmpl-args eshell-did-you-mean blockdiag-mode zotxt zotelo whitespace-cleanup-mode which-key use-package unicode-whitespace tuareg tox tornado-template-mode toc-org test-case-mode systemd system-specific-settings synosaurus sync-recentf sx svg sunshine stickyfunc-enhance ssh-tunnels ssh-config-mode ssh-agency srefactor sphinx-frontend sphinx-doc sparkline spaces sml-mode smartparens scss-mode rainbow-identifiers python-docstring python-django pylint pydoc py-smart-operator py-isort punctuality-logger psgml projectile-speedbar powershell pov-mode php-mode persistent-scratch pdf-tools password-generator pass page-break-lines package+ ox-rst ox-pandoc ox-impress-js outline-magic org-sync org-repo-todo org-projectile org-journal org-doing org-cliplink org-bullets org-beautify-theme org-autolist open-junk-file occur-context-resize never-comment msvc morlock matlab-mode markdown-toc magic-latex-buffer load-relative live-code-talks lisp-extra-font-lock less-css-mode latex-preview-pane latex-pretty-symbols latex-math-preview irony-eldoc inf-ruby imenus ibuffer-vc ibuffer-projectile hy-mode highlight-blocks helm-wordnet helm-w3m helm-w32-launcher helm-pydoc helm-projectile helm-proc helm-make helm-ls-svn helm-ls-git helm-flyspell helm-flycheck helm-emms helm-dictionary helm-descbinds helm-css-scss helm-company helm-bm helm-bibtexkey helm-bibtex goto-chg gnus-desktop-notify gnuplot gnorb git-lens ggo-mode german-holidays geiser fvwm-mode function-args format-sql fontawesome focus flycheck-tip flycheck-pos-tip flycheck-color-mode-line ewmctrl evil-search-highlight-persist eval-in-repl ess-R-data-view eshell-fringe-status erc-view-log erc-track-score erc-social-graph erc-image erc-hl-nicks erc-crypt erc-colorize emoji-cheat-sheet-plus emms-soundcloud emms-mark-ext emms-info-mediainfo electric-operator el-init-viewer easy-escape dsvn django-snippets django-mode dired-toggle dired-rainbow dired-k dired-dups diff-hl dictionary demangle-mode default-text-scale cython-mode csv-mode company-web company-statistics company-restclient company-quickhelp company-math company-irony company-c-headers company-auctex common-lisp-snippets cloc cider cerbere cdlatex calfw-gcal calfw browse-at-remote boxquote bison-mode beginend bash-completion avy auto-dictionary anzu adjust-parens ace-flyspell abc-mode ereader unicode-troll-stopper))
 '(quote
   (safe-local-variable-values
    '((eval setq default-directory
	    (locate-dominating-file default-directory ".dir-locals.el"))
      (projectile-project-name . "Producer-Consumer")
      (projectile-enable-caching . t)
      (ctypes-add-types-at-load "StiffPtr" "Nodes")
      (eval setq default-directory
	    (locate-dominating-file buffer-file-name ".dir-locals.el"))
      (indicate-empty-lines . t)
      (eval org-sbe "setup")
      (engine . jinja2)
      (TeX-engine . latexmk)
      (TeX-engine . pdftex)
      (org-latex-image-default-width . "")
      (eval setq org-latex-default-packages-alist
	    (cons
	     '("mathletters" "ucs" nil)
	     org-latex-default-packages-alist))
      (org-latex-inputenc-alist
       ("utf8" . "utf8x"))
      (c-file-style . kumkeo)
      (eval auto-org-md-on)
      (TeX-command-extra-options . "-shell-escape")
      (todo-categories "Todo")
      (nxml-child-indent . 2)
      (py-use-local-default . t)
      (TeX-master . t)
      (TeX-engine . luatex)
      (TeX-PDF-mode . true)
      (tags-file-name . "../TAGS")
      (tags-file-name . "../../TAGS")
      (encoding . iso-8859-1)
      (sgml-indent-data . t)
      (sgml-indent-step . 1)
      (sgml-default-dtd-file . "~/lib/sgml/docbook.ced")
      (sgml-omittag)
      (sgml-shorttag . t)
      (ctypes-add-types-at-load "PROTOCCALLSFSUB1" "CCALLSFSUB1" "PROTOCCALLSFSUB2" "CCALLSFSUB2" "PROTOCCALLSFSUB3" "CCALLSFSUB3" "PROTOCCALLSFSUB4" "CCALLSFSUB4" "PROTOCCALLSFSUB5" "CCALLSFSUB5" "PROTOCCALLSFSUB6" "CCALLSFSUB6" "PROTOCCALLSFSUB7" "CCALLSFSUB7" "PROTOCCALLSFSUB8" "CCALLSFSUB8" "PROTOCCALLSFSUB9" "CCALLSFSUB9" "PROTOCCALLSFSUB10" "CCALLSFSUB10" "PROTOCCALLSFSUB11" "CCALLSFSUB11" "PROTOCCALLSFSUB12" "CCALLSFSUB12" "PROTOCCALLSFSUB13" "CCALLSFSUB13" "PROTOCCALLSFSUB14" "CCALLSFSUB14" "PROTOCCALLSFSUB15" "CCALLSFSUB15" "PROTOCCALLSFSUB16" "CCALLSFSUB16" "PROTOCCALLSFSUB17" "CCALLSFSUB17")
      (ctypes-add-types-at-load "PyMethodDef" "PyObject" "PyArrayObject" "PyListObject" "Nodes")
      (ctypes-add-types-at-load "PyObject" "PyArrayObject")
      (ispell-dictionary . "german")
      (c-set-style . "gnu")
      (ctypes-add-types-at-load "mxArray")
      (ispell-dictionary . "english"))))
 '(safe-local-variable-values
   '((eval setq-local default-directory
	   (locate-dominating-file buffer-file-name ".dir-locals.el"))
     (eval
      (setq buffer-file-coding-system 'utf-8))
     (c-default-style . "user")
     (mmm-classes jinja2))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "JetBrains Mono" :foundry "JB" :slant normal :weight normal :height 132 :width normal)))))
;; Local Variables:
;; mode: emacs-lisp
;; coding: utf-8
;; End:

;;; emacs-custom.el ends here
