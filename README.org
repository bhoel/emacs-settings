* My personal emacs settings

These are my quite elaborate emacs settings for all kinds of file
types. The settings make use of =el-init= and =use-package= to
organize things.

The whole setup is still quite messy and is changed all the time to
add new features, remove old ones, and further clean the settings.

# Local Variables:
# mode: org
# eval: (auto-org-md-on)
# coding: utf-8
# End:
